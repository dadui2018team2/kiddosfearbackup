Event	ID	Name			Wwise Object Path	Notes
	3119005596	GrandmaQuotes			\Default Work Unit\GrandmaQuotes	

Switch Group	ID	Name			Wwise Object Path	Notes
	3119005596	GrandmaQuotes			\Default Work Unit\GrandmaQuotes	

Switch	ID	Name	Switch Group			Notes
	1274633076	GrandmaQuotes_Outro	GrandmaQuotes			
	2003972975	GrandmaQuotes_Intro	GrandmaQuotes			
	3435840928	GrandmaQuotes_1	GrandmaQuotes			
	3435840930	GrandmaQuotes_3	GrandmaQuotes			
	3435840931	GrandmaQuotes_2	GrandmaQuotes			
	3435840932	GrandmaQuotes_5	GrandmaQuotes			
	3435840933	GrandmaQuotes_4	GrandmaQuotes			
	3435840935	GrandmaQuotes_6	GrandmaQuotes			

State Group	ID	Name			Wwise Object Path	Notes
	2441027675	Language			\Default Work Unit\Language	

State	ID	Name	State Group			Notes
	0	None	Language			
	2193233200	Dansk	Language			
	3383237639	English	Language			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	57799786	GrandmaQuotes 3 Eng	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 3 Eng_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_3\GrandmaQuotes 3 Eng		70274
	132062047	GrandmaQuotes 4 Eng	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 4 Eng_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_4\GrandmaQuotes 4 Eng		34432
	146226397	GrandmaQuotes 6 DK	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 6 DK_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_6\GrandmaQuotes 6 DK		48316
	288076486	GrandmaQuotes 5 DK	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 5 DK_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_5\GrandmaQuotes 5 DK		58664
	313624197	GrandmaQuotes 4 DK	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 4 DK_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_4\GrandmaQuotes 4 DK		48267
	314698009	IntroCinematic DK	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\IntroCinematic DK_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\Intro\IntroCinematic DK		789741
	460348881	GrandmaQuotes 1 DK	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 1 DK_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_1\GrandmaQuotes 1 DK		50853
	519987978	GrandmaQuotes 6 Eng	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 6 Eng_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_6\GrandmaQuotes 6 Eng		57158
	523833921	GrandmaQuotes 2 Eng	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 2 Eng_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_2\GrandmaQuotes 2 Eng		81687
	632496114	GrandmaQuotes 3 DK	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 3 DK_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_3\GrandmaQuotes 3 DK		83762
	674690638	GrandmaQuotes 5 Eng	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 5 Eng_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_5\GrandmaQuotes 5 Eng		60387
	714144497	GrandmaQuotes 1 Eng	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 1 Eng_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_1\GrandmaQuotes 1 Eng		63511
	935048105	Outro DK	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Outro DK_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\Outro\Outro DK		111437
	959803748	GrandmaQuotes 2 DK	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\GrandmaQuotes 2 DK_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\GrandmaQuotes_2\GrandmaQuotes 2 DK		73344
	965782128	IntroCinematic Eng	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\IntroCinematic Eng_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\Intro\IntroCinematic Eng		761403
	973275823	Outro Eng	C:\Users\Dadiu student\Documents\KiddosFear\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Outro Eng_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Grandparent\GrandmaQuotes\Outro\Outro Eng		115783

