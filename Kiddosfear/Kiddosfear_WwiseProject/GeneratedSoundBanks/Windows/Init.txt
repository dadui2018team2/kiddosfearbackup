State Group	ID	Name			Wwise Object Path	Notes
	656861586	Music_Color			\Default Work Unit\Music_Color	
	1090969866	Music_boss			\Default Work Unit\Music_boss	
	2177735725	Music_level			\Default Work Unit\Music_level	
	2441027675	Language			\Default Work Unit\Language	
	2669060037	Music_MenuOutro			\Default Work Unit\Music_MenuOutro	
	2726287257	StateOfGame			\Default Work Unit\StateOfGame	

State	ID	Name	State Group			Notes
	0	None	Music_Color			
	118694655	Colors20	Music_Color			
	135472210	Colors10	Music_Color			
	135472215	Colors15	Music_Color			
	1861816146	Colors5	Music_Color			
	0	None	Music_boss			
	1299194543	ThirdHit	Music_boss			
	1578581236	FirstHit	Music_boss			
	2392906120	SecondHit	Music_boss			
	0	None	Music_level			
	1248196445	MenuOutro	Music_level			
	1290008369	Level_1	Music_level			
	1290008370	Level_2	Music_level			
	1290008371	Level_3	Music_level			
	1290008372	Level_4	Music_level			
	1290008373	Level_5	Music_level			
	1290008374	Level_6	Music_level			
	0	None	Language			
	2193233200	Dansk	Language			
	3383237639	English	Language			
	0	None	Music_MenuOutro			
	1888666919	OutroNoVoice	Music_MenuOutro			
	2607556080	Menu	Music_MenuOutro			
	3650323740	OutroVoice	Music_MenuOutro			
	0	None	StateOfGame			
	984691642	InGame	StateOfGame			
	2607556080	Menu	StateOfGame			

Game Parameter	ID	Name			Wwise Object Path	Notes
	495870151	Sound_Volume			\Sliders\Sound_Volume	
	1006694123	Music_Volume			\Sliders\Music_Volume	
	4179668880	Master_Volume			\Sliders\Master_Volume	

Audio Bus	ID	Name			Wwise Object Path	Notes
	623086306	Sound			\Default Work Unit\Master Audio Bus\Sound	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	
	3991942870	Music			\Default Work Unit\Master Audio Bus\Music	

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

