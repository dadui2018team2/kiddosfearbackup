﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveAnimation : MonoBehaviour {

	public Animator animator;
	private bool running = false;
	
	// Update is called once per frame
	void Update () {
		animator.SetBool("Run", running);
	}

	public void Run(){
		running = true;
	}

	public void Stop(){
		running = false;
	}
}
