﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinNoise : MonoBehaviour {

	public Material[] material;
	public int width = 64;
	public int height = 64;

	public float scale = 20f;

	public float offsetX = 0f;
	public float offsetY = 0f;

	public float offsetSpeed = 0.5f;

	void Update () {
		offsetX += Time.deltaTime * offsetSpeed;
		offsetY += Time.deltaTime * offsetSpeed;
		Texture2D perlinNoiseTarget = GenerateTexture();
		
		for (int i = 0; i < material.Length; i++){
			if (material[i] != null){
				material[i].SetTexture("Texture2D_2AE0E5A9", perlinNoiseTarget);
			}
		}
	}

	private Texture2D GenerateTexture(){
		Texture2D texture = new Texture2D(width, height);

		for (int x = 0; x < width; x++){
			for (int y = 0; y < height; y++){
				Color color = CalculateColor(x, y);
				texture.SetPixel(x, y, color);
			}
		}

		texture.Apply();
		return texture;
	}

	private Color CalculateColor(int x, int y){
		float xCoord = (float)x / width * scale + offsetX;
		float yCoord = (float)y / height * scale + offsetY;

		float sample = Mathf.PerlinNoise(xCoord, yCoord);
		return new Color(sample, sample, sample);
	}
}
