/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID GRANDPARENTQUOTES = 762869842U;
        static const AkUniqueID KIDDODIE = 3753750138U;
        static const AkUniqueID KIDDOFOOTSTEPS = 2266394309U;
        static const AkUniqueID KIDDOPAINTMONSTER = 4186696246U;
        static const AkUniqueID KIDDORESPAWM = 2347096879U;
        static const AkUniqueID MONSTERHITSKIDDO = 1345598762U;
        static const AkUniqueID MONSTERSOUND = 512170010U;
        static const AkUniqueID PAINTINGHITSSURFACE = 3664408292U;
        static const AkUniqueID PICKUPLIGHT = 2491260467U;
        static const AkUniqueID UISOUND = 471286636U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace FOOTSTEPTYPE
        {
            static const AkUniqueID GROUP = 1458816175U;

            namespace SWITCH
            {
                static const AkUniqueID BLUE = 1325827433U;
                static const AkUniqueID GREEN = 4147287986U;
                static const AkUniqueID NORMAL = 1160234136U;
                static const AkUniqueID RED = 980603538U;
            } // namespace SWITCH
        } // namespace FOOTSTEPTYPE

        namespace PAINTINGSURFACE
        {
            static const AkUniqueID GROUP = 1246913898U;

            namespace SWITCH
            {
                static const AkUniqueID BLUE = 1325827433U;
                static const AkUniqueID GREEN = 4147287986U;
                static const AkUniqueID RED = 980603538U;
            } // namespace SWITCH
        } // namespace PAINTINGSURFACE

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID GRANDPARENTQUOTES = 762869842U;
        static const AkUniqueID KIDDODIE = 3753750138U;
        static const AkUniqueID KIDDOFOOTSTEPS = 2266394309U;
        static const AkUniqueID KIDDOPAINTMONSTER = 4186696246U;
        static const AkUniqueID KIDDORESPAWM = 2347096879U;
        static const AkUniqueID MONSTERHITSKIDDO = 1345598762U;
        static const AkUniqueID MONSTERSOUND = 512170010U;
        static const AkUniqueID PAINTINGHITSSURFACE = 3664408292U;
        static const AkUniqueID PICKUPLIGHT = 2491260467U;
        static const AkUniqueID UISOUND = 471286636U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
