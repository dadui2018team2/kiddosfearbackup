﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleDoorActivator : MonoBehaviour {

    public DoorInteraction myDoor;

    private void Start() {
        myDoor = transform.parent.GetComponent<DoorInteraction>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            myDoor.AttemptActivateDoor();
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
