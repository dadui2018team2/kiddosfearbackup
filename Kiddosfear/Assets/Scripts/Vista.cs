﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vista : MonoBehaviour {

    [Header("Referenes are set by the script")]
    [SerializeField] Transform vistaPos;
    [SerializeField] CameraController camControl;
    //[SerializeField] CameraAnimation camAnim;
    //[SerializeField] UIGameManager uiControl;
    //[SerializeField] TouchPaint touchPaint;

    private bool panning = false;

	// Use this for initialization
	private void Awake () {
        vistaPos = gameObject.transform.GetChild(0).transform;
        camControl = FindObjectOfType<CameraController>();

        //----------------saved in case we need more control during the camera panning---------------------------
        //camAnim = FindObjectOfType<CameraAnimation>();
        //uiControl = FindObjectOfType<UIGameManager>();
        //touchPaint = FindObjectOfType<TouchPaint>();
    }

    //When the player enters the trigger. set the new focuspoint to the child of the vista gameobject
    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            camControl._world = vistaPos.gameObject;
        }

        //----------------saved in case we need more control during the camera panning---------------------------
        /*if (!panning) {
            camAnim.Pan(gameObject.transform, vistaPos, 0f, 0f);
            uiControl.ShowGUI(true);
            touchPaint.enabled = true;
            StartCoroutine(WaitForPan());
        }*/

    }

    //When the player exits the trigger set the focuspoint back to the player
    private void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            camControl._world = GameManager.Player;
        }
    }


    //----------------saved in case we need more control during the camera panning---------------------------
    /*IEnumerator WaitForPan() {
        panning = true;
        int counter = 0;
        while (camAnim.isPanning || counter < 100) {
            yield return new WaitForEndOfFrame();
            counter++;
        }
        counter = 0;
        camControl._world = vistaPos.gameObject;
    }*/


}
