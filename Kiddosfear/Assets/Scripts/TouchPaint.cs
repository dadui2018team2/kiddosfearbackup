﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TouchPaint : MonoBehaviour {

    [SerializeField] GameObject sparklesObject;
    [SerializeField] GameObject bubblesObject;
    private ParticleSystem sparkles;
    private ParticleSystem bubbles;
    public ParticleSystem penColor;

    [Header("Paint line")]
    private LineRenderer paintLine;
    private List<Vector3> lineRendererPositions = new List<Vector3>();

    [Tooltip("The higher the number, the longer the paintline")]
    [SerializeField] int linePositionsToSave;

    [Range(0f, 1f)]
    [SerializeField] float lineStartWidth;
    [Range(0f, 1f)]
    [SerializeField] float lineEndWidth;

    [SerializeField] Gradient redGradient;
    [SerializeField] Gradient greenGradient;
    [SerializeField] Gradient blueGradient;
    [SerializeField] Gradient eraseGradient;

    [Space(10)]
    [SerializeField] Color redEmission;
    [SerializeField] Color greenEmission;
    [SerializeField] Color blueEmission;

    //Other variables to be used to color the world
    private int layerMask = 1 << 9;
    private int trackedFingerId = -1;
    [HideInInspector] public string currentColor;

    private Vector3 previousCameraPosition;
    private Vector3 currentCameraPosition;

    //Variables for detecting whether the mouse/touch is on a GUI-element
    GraphicRaycaster graphicRaycaster;
    PointerEventData pointerEventData = new PointerEventData(null);
    List<RaycastResult> graphicsRaycastResult = new List<RaycastResult>();

    private void Awake() {
        //Automatically picking the color that was previously picked
        if (!PlayerPrefs.HasKey("CurrentColor"))
            PlayerPrefs.SetString("CurrentColor", "None");

        SelectColor(PlayerPrefs.GetString("CurrentColor"));

        paintLine = GetComponent<LineRenderer>();
        paintLine.positionCount = linePositionsToSave;
        paintLine.startWidth = lineStartWidth;
        paintLine.endWidth = lineEndWidth;

        sparkles = sparklesObject.GetComponent<ParticleSystem>();
        if (sparkles.main.customSimulationSpace == null)
            Debug.LogError("No custom simulation space set for TouchPaint sparkles. Please put the main camera as the custom simulation space.");

        bubbles = bubblesObject.GetComponent<ParticleSystem>();
        if (bubbles.main.customSimulationSpace == null)
            Debug.LogError("No custom simulation space set for TouchPaint bubbles. Please put the main camera as the custom simulation space.");

        previousCameraPosition = Camera.main.transform.position;

        graphicRaycaster = FindObjectOfType<GraphicRaycaster>();
    }

    private void Update() {
        currentCameraPosition = Camera.main.transform.position;
        Vector2 input = GetInput();

        if (input.magnitude == 0) {
            ShowPaintLine(false);
            previousCameraPosition = currentCameraPosition;
            return;
        }

        if (input.magnitude > 0 && currentColor != "None") {
            Ray ray = Camera.main.ScreenPointToRay(input);
            RaycastHit hit;

            //Updating the paint line
            ShowPaintLine(true);
            UpdatePaintLine(ray.origin + ray.direction);

            if (Physics.Raycast(ray.origin, ray.direction, out hit)) {
                if ((layerMask & 1 << hit.collider.gameObject.layer) == 1 << hit.collider.gameObject.layer) {
                    //Coloring the cube that was hit
                    if (hit.collider.tag != currentColor) {
                        hit.collider.gameObject.GetComponent<CubeColoring>().ChangeColor(currentColor);
                    }
                }
            }
        }
        else {
            ShowPaintLine(false);
        }

        previousCameraPosition = currentCameraPosition;
    }

    private Vector2 GetInput() {
        Vector2 input = new Vector2(0f, 0f);

#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetMouseButton(0) && PointerIsOnGUI(Input.mousePosition) == false) {
            input = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
#elif UNITY_ANDROID
        //If we are not coloring right now, check if there are viable touches to start tracking and coloring with
        if (trackedFingerId == -1) {
            for (int i = 0; i < Input.touchCount; i++) {
                Touch touch = Input.GetTouch(i);
                if (touch.phase.Equals(TouchPhase.Began) && PointerIsOnGUI(touch.position) == false) {
                    trackedFingerId = touch.fingerId;
                    input = touch.position;
                }
            }
        }
        //If we are currently coloring, update the input position and check whether we stopped coloring this frame
        else {
            for (int i = 0; i < Input.touchCount; i++) {
                Touch touch = Input.GetTouch(i);
                if (touch.fingerId == trackedFingerId) {
                    if (touch.phase.Equals(TouchPhase.Ended) || PointerIsOnGUI(touch.position)) {
                        trackedFingerId = -1;
                    }
                    else{
                        input = touch.position;
                    }
                }
            }
        }
#endif

        return input;
    }

    private void UpdatePaintLine(Vector3 newPosition) {
        if (lineRendererPositions.Count < linePositionsToSave) {
            for (int i = 0; i < linePositionsToSave; i++) {
                lineRendererPositions.Add(newPosition);
            }
        }

        lineRendererPositions.Add(newPosition);

        if (lineRendererPositions.Count > linePositionsToSave) {
            lineRendererPositions.RemoveAt(0);
        }

        if (Vector3.Distance(previousCameraPosition, currentCameraPosition) > 0.01f) {
            lineRendererPositions = ShiftPositionsWithCamera(lineRendererPositions);
        }

        if(currentColor == "")
            bubblesObject.transform.position = lineRendererPositions[lineRendererPositions.Count - 1];
        else
            sparklesObject.transform.position = lineRendererPositions[lineRendererPositions.Count - 1];

        paintLine.SetPositions(lineRendererPositions.ToArray());
    }

    private List<Vector3> ShiftPositionsWithCamera(List<Vector3> positions) {
        Vector3 shift = currentCameraPosition - previousCameraPosition;

        for (int i = 0; i < positions.Count; i++) {
            positions[i] += shift;
        }

        return positions;
    }

    private void ShowPaintLine(bool show) {
        if (show && paintLine.enabled == false) {
            paintLine.enabled = true;

            switch (currentColor) {
                case "Red":
                    sparkles.Play();
                    penColor.Play();
                    paintLine.colorGradient = redGradient;
                    paintLine.material.SetColor("_EmissionColor", redEmission);
                    sparkles.startColor = Color.red;
                    penColor.startColor = new Color(0.9f, 0.2f, 0.2f, 1);
                    break;
                case "Green":
                    sparkles.Play();
                    penColor.Play();
                    paintLine.colorGradient = greenGradient;
                    paintLine.material.SetColor("_EmissionColor", greenEmission);
                    sparkles.startColor = Color.green;
                    penColor.startColor = new Color(0.2f, 0.9f, 0.2f, 1);
                    break;
                case "Blue":             
                    sparkles.Play();
                    penColor.Play();
                    paintLine.colorGradient = blueGradient;
                    paintLine.material.SetColor("_EmissionColor", blueEmission);
                    sparkles.startColor = Color.blue;
                    penColor.startColor = new Color(0.2f, 0.2f, 0.9f, 1);
                    break;
                case "":
                    bubbles.Play();
                    paintLine.colorGradient = eraseGradient;
                    paintLine.material.SetFloat("_EmissionEnabled", 0f);
                    sparkles.Stop();
                    break;
                default:
                    paintLine.colorGradient = new Gradient();
                    break;
            }
        }
        else if (show == false && paintLine.enabled == true) {
            penColor.Stop();
            bubbles.Stop();
            sparkles.Stop();
            lineRendererPositions.Clear();
            paintLine.enabled = false;
        }
    }

    private bool PointerIsOnGUI(Vector2 input) {
        graphicsRaycastResult.Clear();

        pointerEventData.position = new Vector3(input.x, input.y, 0f);
        graphicRaycaster.Raycast(pointerEventData, graphicsRaycastResult);

        if (graphicsRaycastResult.Count > 0)
            return true;
        else
            return false;
    }

    /// <summary>
    /// Selecting the color/material to put on cubes and projectiles via a string
    /// </summary>
    /// <param name="color">Red, Green or Blue</param>
    public void SelectColor(string color) {
        if(color != "None")
            AudioPlayer.PlayEvent("ActivationOfPaint", gameObject, 3f);

        currentColor = color;
        PlayerPrefs.SetString("CurrentColor", color);
    }
}
