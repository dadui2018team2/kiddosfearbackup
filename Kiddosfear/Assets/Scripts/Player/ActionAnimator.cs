﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionAnimator : MonoBehaviour {

    private GameObject player;
    private Animator anim;
    private CharacterController cc;
    private PlayerMovement movement;
    //private Shooting shootMechanic;

    public float actionBlendSpeed;
    private float fullBodyLayerWeight = 0f;
    private float upperBodyLayerWeight = 0f;

    private void Awake() {
        player = GameObject.FindWithTag("Player");
        anim = player.GetComponent<Animator>();
        cc = player.GetComponent<CharacterController>();
        movement = player.GetComponent<PlayerMovement>();
        //shootMechanic = player.GetComponent<Shooting>(); //Use this in case of shooting animation
    }

    private void Update() {
        anim.SetBool("isGrounded", cc.isGrounded);
        anim.SetFloat("yVelocity", cc.velocity.y);
        anim.SetBool("gravity", movement.applyGravity);
        anim.SetBool("shouldSlide", movement.shouldSlide);

        //anim.SetBool("hasShot", shootMechanic.hasShot); //Use this in case of shooting animation
        //anim.SetFloat("currentShotMagnitude", shootMechanic.currentShotMagnitude); //Use this in case of shooting animation

        UpdateFullBodyLayerWeight();
        UpdateUpperBodyLayerWeight();
    }

    private void UpdateFullBodyLayerWeight() {
        //Activating the action layer if: jumping/falling, 
        if (!anim.GetCurrentAnimatorStateInfo(1).IsName("idle")) {
            fullBodyLayerWeight += actionBlendSpeed;
            fullBodyLayerWeight = Mathf.Clamp(fullBodyLayerWeight, 0f, 1f);
        }
        //If we're currently not in a transition, and we're not playing "land", "celebration_EndOfLevel" nor "death", fade out the action layer
        else if (!anim.IsInTransition(1)
            && !anim.GetCurrentAnimatorStateInfo(1).IsName("land")
            && !anim.GetCurrentAnimatorStateInfo(1).IsName("celebration_EndOfLevel")
            && !anim.GetCurrentAnimatorStateInfo(1).IsName("death")) {
            fullBodyLayerWeight -= actionBlendSpeed;
            fullBodyLayerWeight = Mathf.Clamp(fullBodyLayerWeight, 0f, 1f);
        }

        anim.SetLayerWeight(1, fullBodyLayerWeight);
    }

    private void UpdateUpperBodyLayerWeight() {
        //Activating upper body layer if we're doing any other action than idling
        if (!anim.GetCurrentAnimatorStateInfo(2).IsName("idle")) {
            upperBodyLayerWeight = 1f;
        }
        else if (!anim.IsInTransition(2)) {
            upperBodyLayerWeight -= actionBlendSpeed;
            upperBodyLayerWeight = Mathf.Clamp(upperBodyLayerWeight, 0f, 1f);
        }

        anim.SetLayerWeight(2, upperBodyLayerWeight);
    }

    public void TriggerDeathAnim() {
        fullBodyLayerWeight = 1f;
        anim.SetTrigger("death");
    }

    public void TriggerCelebrationAnim() {
        fullBodyLayerWeight = 1f;
        anim.SetTrigger("celebration");
    }

    public void TriggerHitAnim() {
        upperBodyLayerWeight = 1f;
        anim.SetTrigger("gotHit");
    }

    public bool HasFinishedAnimation(string name) {
        switch (name) {
            case "death":
                if (anim.GetBool("death") == false && anim.GetCurrentAnimatorStateInfo(1).normalizedTime > 0.95f)
                    return true;
                else
                    return false;

            case "celebration":
                if (anim.GetBool("celebration") == false && anim.GetCurrentAnimatorStateInfo(1).normalizedTime > 0.95f)
                    return true;
                else
                    return false;

            default:
                Debug.LogError("No animation clip named " + name + ".");
                return false;
        }
    }

    public bool LocomotionLayerHidden() {
        if (fullBodyLayerWeight > 0.5f)
            return true;
        else
            return false;
    }
}
