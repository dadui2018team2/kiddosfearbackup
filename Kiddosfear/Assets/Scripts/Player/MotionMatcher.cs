﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotionMatcher : MonoBehaviour
{
    /*
     * ----------TODO:----------
     * 1) Consider other ways of calculating the velocity dissimilarity. Consider the ending points! Maybe just calculate the trajectory-similarity between the current clip and the player clip?
     * 2) Don't have the joystick snap to 1 when pressing forward. I'd like it to be floaty.
     * -------------------------
     */

    [Header("General")]
    private GameObject character;
    //public GameObject character;
    //public Joystick movementJoystick;
    public bool disableMotionMatching;
    public bool disableRootMotion;

    public AnimationClip[] clips;

    private Transform root_bone;
    private CharacterController characterController;
    private Animator animator;
    private Vector3 original_pos;
    private Quaternion original_rot;
    private Text animText;
    private int idleAnimIndex = -1;

    [Header("Trajectories")]
    [Tooltip("How many seconds of each clip should be sampled?")]
    public float secondsToSample;
    [Tooltip("How many frames should be skipped between each trajectory sample?")]
    public int framesToSkip;
    public AnimationCurve characterTrajectoryPredictiveCurve = new AnimationCurve();
    [Range(0f, 0.5f)]
    public float characterTrajectorySmoothening;

    private PlayerMovement playerMovement;
    private Vector3[] characterTrajectory;
    private Vector3[] previousCharacterTrajectory;
    private Vector3[][] clipTrajectories;
    private float frameRate = 30f;

    [Header("Movement and animation")]
    [Range(1f, 5f)]
    public float velocityShortening;
    public float idleThreshold;
    public float allowedVelocityDeviation;
    public float animationSwitchCooldown;

    private int currentClip;
    private int nearestClip;
    private float currentCooldown;

    [Header("Debugging")]
    public bool writeCurrentAnimation;

    [Space(10)]
    public Vector3 currentVelocity;
    public Vector3 desiredVelocity;
    public float velocityDeviation;

    [Space(10)]
    public bool drawCharacterTrajectory;
    public bool drawClipTrajectories;
    public bool drawCurrentClipTrajectory;
    public bool drawComparedSamples;
    public bool showTrajectorySections;
    public bool trajectoriesFollow;
    public bool trajectoriesRotate;

    //FUNCTION: Initializing variables.
    private void Awake() {
        character = GameObject.FindWithTag("Player");
        characterController = character.GetComponent<CharacterController>();
        playerMovement = character.GetComponent<PlayerMovement>();
        animator = character.GetComponent<Animator>();
        //clips = animator.runtimeAnimatorController.animationClips;
        root_bone = animator.GetBoneTransform(HumanBodyBones.Hips).transform;
        //animText = GameObject.FindWithTag("AnimationText").GetComponent<Text>();
        //movementJoystick = FindObjectOfType<Joystick>();

        clipTrajectories = new Vector3[clips.Length][];

        original_pos = character.transform.position;
        original_rot = character.transform.rotation;
    }

    //FUNCTION: Mainly calculating clip trajectories (which can be done offline and be saved in a database)
    private void Start() {
        for (int i = 0; i < clips.Length; i++) {
            clipTrajectories[i] = PredictClipTrajectory(clips[i].name, secondsToSample, framesToSkip);

            if (idleAnimIndex == -1 && clips[i].name == "idle") {
                idleAnimIndex = i;
            }
        }

        if (idleAnimIndex == -1)
            Debug.LogError("No animation named \'idle\' was found!");

        if (disableRootMotion) {
            animator.applyRootMotion = false;
        }
        else
            FindObjectOfType<PlayerMovement>().enabled = false;

        animator.Play(clips[idleAnimIndex].name);
    }

    private void Update() {
        UpdateVelocities();
        characterTrajectory = PredictCharacterTrajectory(0.1f, velocityShortening, characterTrajectorySmoothening);
        velocityDeviation = CalculateVelocityDeviation(true, 0.5f);

        if (!disableMotionMatching) {
            //Idling
            if (desiredVelocity.magnitude < idleThreshold && currentClip != idleAnimIndex) {
                TransitionToClip(idleAnimIndex);
            }
            //Switching animation
            else if (/*velocityDeviation > allowedVelocityDeviation &&*/ currentCooldown <= 0f) {
                nearestClip = FindMostSimilarClipTrajectory();
                if (nearestClip != currentClip) {
                    TransitionToClip(nearestClip);
                    StartCoroutine("StartCooldown");
                }
            }

            /*
            if (writeCurrentAnimation)
                animText.text = clips[currentClip].name;
            else
                animText.text = "";
                */
        }

        DrawDebugLines();
    }

    //FUNCTION: Predicts the character's trajectory based on the animation curve. Samples will always be calculated from position(0,1,0) and rotation (0,0,0).
    private Vector3[] PredictCharacterTrajectory(float interpolationStep, float velocityDivision, float smoothening) {
        List<Vector3> samples = new List<Vector3>();

        Vector3 calculatedPosition = new Vector3(0, 1, 0);
        Vector3 calculatedVelocity = Vector3.zero;

        for (float i = 0f; i <= 1f; i += interpolationStep) {
            //Smoothening
            if (previousCharacterTrajectory != null)
                calculatedPosition = Vector3.Lerp(calculatedPosition, previousCharacterTrajectory[samples.Count], smoothening);

            samples.Add(calculatedPosition);

            //Lerping between current and desired velocity, and applying the resulting velocity to the calculated position.
            calculatedVelocity = Vector3.Lerp(currentVelocity, desiredVelocity, characterTrajectoryPredictiveCurve.Evaluate(i));
            calculatedPosition += calculatedVelocity / velocityDivision; //NOTE: velocityDivision can be used to shorten the player trajectory to fit with the clip trajectories.
            calculatedPosition.y = 1f; //NOTE: resetting y-component on calculatedPosition because I don't need to consider up/down-movement.
        }

        previousCharacterTrajectory = samples.ToArray();

        return previousCharacterTrajectory;
    }

    //FUNCTION: Samples the root position of the character during the given clip. Samples will always be calculated from position(0,1,0) and rotation (0,0,0).
    private Vector3[] PredictClipTrajectory(string clip, float secondsToSample, int framesToSkip) {
        List<Vector3> samples = new List<Vector3>();

        //Temporarily moving the character to (0,1,0) and with no rotation for the duration of the clip trajectory calculation.
        character.transform.position = new Vector3(0, 1, 0);
        character.transform.rotation = new Quaternion(0, 0, 0, 1);

        //Calculating the total frames in the given clip and which frame to stop sampling at.
        animator.Play(clip, 0, 0f);
        animator.Update(0f);
        float totalFrames = Mathf.Floor(animator.GetCurrentAnimatorStateInfo(0).length * frameRate);
        int lastFrameToSample = Mathf.FloorToInt(secondsToSample * frameRate);

        //Capping "lastFrameToSample" to be at max the total frames in the clip, since
        //it could risk being above totalFrames if "secondsToSample" is larger than the total clip length.
        if (lastFrameToSample > totalFrames)
            lastFrameToSample = (int)totalFrames;

        for (int frame = 0; frame <= lastFrameToSample; frame += framesToSkip) {
            animator.Play(clip, 0, (float)frame / totalFrames);
            animator.Update(framesToSkip / frameRate);
            samples.Add(root_bone.position);
        }

        //Returning the character to his starting point.
        animator.Play(clip, 0, 1f);
        animator.Update(1f);
        character.transform.position = original_pos;
        character.transform.rotation = original_rot;

        return samples.ToArray();
    }

    //FUNCTION: Compares the next, midway and end samples of the character trajectory and each clip trajectory, and returns the index of the most similar clip trajectory.
    private int FindMostSimilarClipTrajectory() {
        int mostSimilarTrajectory = 0;
        float smallestDissimilarity = Mathf.Infinity;

        for (int i = 0; i < clipTrajectories.Length; i++) {
            //Hacky way of avoiding accidently transitioning to the idle-animation when the character is supposed to move.
            if (i == idleAnimIndex && desiredVelocity.magnitude > idleThreshold)
                continue;

            float dissimilarity = CalculateTrajectoryDissimilarity(characterTrajectory, clipTrajectories[i]);
            if (dissimilarity < smallestDissimilarity) {
                smallestDissimilarity = dissimilarity;
                mostSimilarTrajectory = i;
            }
        }

        return mostSimilarTrajectory;
    }

    //FUNCTION: Compares the distance between certain samples in two trajectories, returning the sum of distances (i.e. the dissimilarity).
    private float CalculateTrajectoryDissimilarity(Vector3[] trajectory1, Vector3[] trajectory2) {
        float dissimilarity = 0f;

        //1. Compare next sample, 2. Compare midway sample, 3. Compare end sample.
        dissimilarity += Vector3.Distance(trajectory1[1], trajectory2[1]);
        dissimilarity += Vector3.Distance(trajectory1[Mathf.FloorToInt((float)trajectory1.Length / 2f)], trajectory2[Mathf.FloorToInt((float)trajectory2.Length / 2f)]);
        dissimilarity += Vector3.Distance(trajectory1[trajectory1.Length - 1], trajectory2[trajectory2.Length - 1]);

        return dissimilarity;
    }

    //FUNCTION: Draws the debug-lines that have been enabled in the inspector.
    private void DrawDebugLines() {
        if (drawCharacterTrajectory) {
            DrawTrajectory(characterTrajectory, Color.white);
        }

        if (drawClipTrajectories) {
            foreach (Vector3[] trajectory in clipTrajectories) {
                DrawTrajectory(trajectory, Color.red);
            }
        }

        if (drawCurrentClipTrajectory) {
            DrawTrajectory(clipTrajectories[currentClip], Color.green);
        }

        if (drawComparedSamples) {
            if (trajectoriesFollow) {
                Debug.DrawLine(characterTrajectory[1] + character.transform.position, clipTrajectories[currentClip][1] + character.transform.position, Color.magenta);
                Debug.DrawLine(characterTrajectory[Mathf.FloorToInt((float)characterTrajectory.Length / 2f)] + character.transform.position, clipTrajectories[currentClip][Mathf.FloorToInt((float)clipTrajectories[currentClip].Length / 2f)] + character.transform.position, Color.magenta);
                Debug.DrawLine(characterTrajectory[characterTrajectory.Length - 1] + character.transform.position, clipTrajectories[currentClip][clipTrajectories[currentClip].Length - 1] + character.transform.position, Color.magenta);
            }
            else {
                Debug.DrawLine(characterTrajectory[1], clipTrajectories[currentClip][1], Color.magenta);
                Debug.DrawLine(characterTrajectory[Mathf.FloorToInt((float)characterTrajectory.Length / 2f)], clipTrajectories[currentClip][Mathf.FloorToInt((float)clipTrajectories[currentClip].Length / 2f)], Color.magenta);
                Debug.DrawLine(characterTrajectory[characterTrajectory.Length - 1], clipTrajectories[currentClip][clipTrajectories[currentClip].Length - 1], Color.magenta);
            }
        }
    }

    //FUNCTION: Draws a debug line between all the given samples.
    private void DrawTrajectory(Vector3[] samples, Color primaryColor) {
        Vector3 p1;
        Vector3 p2;

        Color secondaryColor = primaryColor / 3f;
        secondaryColor.a = 1f;
        Color[] colors = new Color[] { primaryColor, secondaryColor };

        for (int i = 1; i < samples.Length; i++) {
            p1 = samples[i - 1];
            p2 = samples[i];

            p1 += transform.position;
            p2 += transform.position;

            //If trajectories should rotate with the character, add his rotation to the points.
            if (trajectoriesRotate) {
                p1 = character.transform.rotation * p1;
                p2 = character.transform.rotation * p2;
            }

            //If trajectories should follow the character, add his position to the points.
            if (trajectoriesFollow) {
                p1 += root_bone.transform.position;
                p2 += root_bone.transform.position;

                p1.y -= root_bone.transform.position.y;
                p2.y -= root_bone.transform.position.y;
            }

            if (showTrajectorySections)
                Debug.DrawLine(p1, p2, colors[i % 2]);
            else
                Debug.DrawLine(p1, p2, primaryColor);
        }
    }

    //FUNCTION: Gets the player input from the on-screen joystick, or alternatively from the horizontal/vertical input axes.
    private Vector3 GetPlayerInput() {
        Vector3 input = Vector3.zero;

        if (playerMovement.walkThroughDoor)
            return Vector3.forward;

        input = playerMovement.baseInput;

        //If the virtual joystick is not in use, maybe a controller is plugged in and is being used instead?
        //if (input == Vector3.zero)
        //    input = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

        return input;
    }

    //FUNCTION: Setting currentVelocity and desiredVelocity based on controller input and the velocity of the character controller.
    private void UpdateVelocities() {
        currentVelocity = Vector3.ClampMagnitude(characterController.velocity, 1f);
        desiredVelocity = Vector3.ClampMagnitude(GetPlayerInput(), 1f);

        //Removing character rotation from the velocities, as all trajectories are calculated from position (0,1,0) with no rotation.
        desiredVelocity = Quaternion.Inverse(character.transform.rotation) * desiredVelocity;
        currentVelocity = Quaternion.Inverse(character.transform.rotation) * currentVelocity;
    }

    //FUNCTION: Calculating the distance between currentVelocity and desiredVelocity.
    private float CalculateVelocityDeviation(bool ignoreYVelocity, float maxVelocityMagnitude) {
        return Vector3.Distance(Vector3.ClampMagnitude(currentVelocity, maxVelocityMagnitude), Vector3.ClampMagnitude(desiredVelocity, maxVelocityMagnitude));
    }
    
    //FUNCTION: Crossfades to the clip identified by clipIndex.
    private void TransitionToClip(int clipIndex) {
        //animator.Play(clips[clipIndex].name);
        animator.CrossFade(clips[clipIndex].name, 0.08f);
        currentClip = clipIndex;
    }

    //FUNCTION: Counts down from 'animationSwitchCooldown' to 0.
    IEnumerator StartCooldown() {
        currentCooldown = animationSwitchCooldown;
        while (currentCooldown > 0f) {
            currentCooldown -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    #region Currently unused code
    /*
    //FUNCTION: Predicts the character's trajectory based on the animation curve. Samples will always be calculated from position(0,1,0) and rotation (0,0,0).
    private Vector3[] PredictCharacterTrajectory(float curveEvaluationStep, float velocityDivision, float smoothening) {
        List<Vector3> samples = new List<Vector3>();
        float interpolationStep = 0.1f; //framesToSkip / (secondsToSample * frameRate);

        Vector3 calculatedPosition = new Vector3(0, 1, 0);
        Vector3 calculatedVelocity = Vector3.zero;

        float curveEvaluation = 0f;

        for (float i = 0f; i <= 1f; i += interpolationStep) {
            //Smoothening
            if (previousCharacterTrajectory != null)
                calculatedPosition = Vector3.Lerp(calculatedPosition, previousCharacterTrajectory[samples.Count], smoothening);

            samples.Add(calculatedPosition);

            //Lerping between current and desired velocity, and applying the resulting velocity to the calculated position.
            calculatedVelocity = Vector3.Lerp(currentVelocity, desiredVelocity, characterTrajectoryPredictiveCurve.Evaluate(i));//curveEvaluation));
            calculatedPosition += calculatedVelocity / velocityDivision; //NOTE: velocityDivision can be used to shorten the player trajectory to fit with the clip trajectories.
            calculatedPosition.y = 1f; //NOTE: resetting y-component on calculatedPosition because I don't need to consider up/down-movement.

            //Increasing curveEvaluation to evaluate next step of the animation curve, and clamping it to 1.
            curveEvaluation += curveEvaluationStep;
            if (curveEvaluation > 1f)
                curveEvaluation = 1f;
        }

        previousCharacterTrajectory = samples.ToArray();

        return previousCharacterTrajectory;
    }*/
    #endregion
}