﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    #region Variables
    [Header("General")]
    [Tooltip("How many degrees from direct forward movement is accepted as rotation?")]
    [SerializeField] float rotationThreshold;

    [HideInInspector]
    public bool applyGravity;
    [HideInInspector]
    public bool moveLeft;
    [HideInInspector]
    public bool moveRight;

    [Header("Ground movement")]
    [Tooltip("The maximum speed allowed on ground.")]
    [SerializeField] float moveSpeedGrounded;
    [Tooltip("The turning speed on ground.")]
    [SerializeField] float turnSpeedGrounded;
    [Tooltip("The acceleration/decceleration rate of the player's speed on ground.")]
    [SerializeField] float accelerationGrounded;

    [Header("Air movement")]
    [Tooltip("The maximum speed allowed in air.")]
    [SerializeField] float moveSpeedAirborne;
    [Tooltip("The turning speed in air.")]
    [SerializeField] float turnSpeedAirborne;
    [Tooltip("The acceleration/decceleration rate of the player's speed in air.")]
    [SerializeField] float accelerationAirborne;

    [Header("Enemy pushback")]
    [Tooltip("Magnitude of force recieved when hit by enemy.")]
    [SerializeField] float enemyPushbackForce;
    [Tooltip("Deceleration of pushback.")]
    [SerializeField] float enemyPushbackDeceleration;

    [Header("Wedge sliding")]
    [Tooltip("How fast should the player slide off of wedges?")]
    public float slideSpeed;
    [HideInInspector] public bool shouldSlide;
    private bool wasSliding;
    private Vector3 hitNormal;
    private Vector3 lastSlideHitNormal;
    private float slideTime = 0f;

    [Header("Dust particles")]
    public GameObject dustParticles;
    public GameObject dustParticlesRun;

    //A reference to the latest blue cube that was touched, for custom jump-force if Kiddo is on the edge of a blue cube.
    private GameObject blueCube = null;

    //A struct for saving movement variables in (i.e. saving a set of ground-variables OR air-variables to be used in functions)
    struct MovementVariables {
        public float moveSpeed;
        public float turnSpeed;
        public float acceleration;
    }

    //Components that we need a reference to
    private Transform player;
    private Transform mainCamera;
    private CharacterController characterController;
    private CubeInteraction cubeInteraction;
    private CameraAnimation cameraAnimation;

    //General variables for calculating the moveDirection
    private MovementVariables movement = new MovementVariables();
    private float acceleratedMoveSpeed = 0f;
    private float acceleratedGravityPull = 0f;
    private Vector3 moveDirection;
    public Vector3 baseInput = Vector3.zero;

    //Variables for cube interactions
    private Vector3 redPull;
    private Vector3 targetJumpForce;
    private Vector3 currentJumpForce;
    private float greenMultiplier;

    //Enemy pushback force
    private Vector3 enemyForce = Vector3.zero;

    [HideInInspector]
    public bool walkThroughDoor;

    #endregion

    private void Awake() {
        player = GameObject.FindWithTag("Player").transform;
        mainCamera = Camera.main.transform;
        characterController = player.gameObject.GetComponent<CharacterController>();
        cubeInteraction = GetComponent<CubeInteraction>();
        targetJumpForce = Vector3.zero;

        cameraAnimation = mainCamera.GetComponent<CameraAnimation>();

        moveLeft = false;
        moveRight = false;
    }

    private void FixedUpdate() {
        //Update movement variables based on whether we're in the air or on the ground, and boost the max speed with the currebt green cube boost..
        UpdateMovementVariables();
        cubeInteraction.DetectCubes();

        greenMultiplier = cubeInteraction.GetGreenMultiplier();
        //Check if we are on a green if we are activate dust particles behind the player
        if (cubeInteraction.greenSuccession && characterController.velocity.magnitude > 0.05f)
            ActivateGreenDust();
        else
            DeactivateGreenDust();
        movement.moveSpeed *= greenMultiplier;

        //Now calculate the general move direction (which takes the boosted top speed into account).
        CalculateMovementDirection();

        //Add the blue force to the movement direction.
        moveDirection += LerpedJumpForce();

        //Slide down an uncolored wedge if you're on one - overrides the regular movement (would be overriden by enemies pushing the player).
        if (shouldSlide) {
            moveDirection.x = ((1f - hitNormal.y) * hitNormal.x) * slideSpeed;
            moveDirection.y = acceleratedGravityPull - (((1f - hitNormal.y) * hitNormal.y) * slideSpeed);
            moveDirection.z = ((1f - hitNormal.y) * hitNormal.z) * slideSpeed;
            lastSlideHitNormal = hitNormal;
            wasSliding = true;
        }
        else if (wasSliding) {
            slideTime += Time.deltaTime;
            moveDirection.x = ((1f - lastSlideHitNormal.y) * lastSlideHitNormal.x) * slideSpeed;
            moveDirection.y = acceleratedGravityPull - (((1f - lastSlideHitNormal.y) * lastSlideHitNormal.y) * slideSpeed);
            moveDirection.z = ((1f - lastSlideHitNormal.y) * lastSlideHitNormal.z) * slideSpeed;

            if (slideTime > 0.25f) {
                slideTime = 0f;
                wasSliding = false;
            }
        }

        // Apply pushback if there is any.
        if (enemyForce != Vector3.zero){
            moveDirection = new Vector3(enemyForce.x, moveDirection.y, enemyForce.z);
            if (enemyForce.magnitude < enemyPushbackDeceleration * Time.deltaTime) enemyForce = Vector3.zero;
            else enemyForce -= enemyForce.normalized * enemyPushbackDeceleration * Time.deltaTime;
        }

        //Make sure the player is not moving in the z-axis unless they're walking to the exit door.
        if(!walkThroughDoor) {
            moveDirection.z = 0;
        }

        //Now move the character in the calculated direction and rotate him to face the direction he's moving in.
        characterController.Move(moveDirection * Time.deltaTime);
        RotateCharacter();

        if (cubeInteraction.lastKnownGroundCube != null) {
            if (cubeInteraction.isOnWedge && Vector3.Angle(Vector3.up, hitNormal) > characterController.slopeLimit) {
                shouldSlide = true;
            }
            else {
                characterController.slopeLimit = cubeInteraction.defaultSlopeLimit;
                shouldSlide = false;
            }
            /*
            if (cubeInteraction.lastKnownGroundCube.tag == "Wedge" && Vector3.Angle(Vector3.up, hitNormal) > characterController.slopeLimit)
                shouldSlide = true;
            else
                shouldSlide = false;
                */
        }
    }

    /// <summary>
    /// Updating the "movement"-variable with either grounded or airborne values based on CharacterController.isGrounded.
    /// </summary>
    private void UpdateMovementVariables() {
        if (characterController.isGrounded) {
            movement.moveSpeed = moveSpeedGrounded;
            movement.turnSpeed = turnSpeedGrounded;
            movement.acceleration = accelerationGrounded;
        }
        else {
            movement.moveSpeed = moveSpeedAirborne;
            movement.turnSpeed = turnSpeedAirborne;
            movement.acceleration = accelerationAirborne;
        }
    }

    /// <summary>
    /// Calculate the final moveDirection based on joystick input, gravity and whether you're standing on a green cube (which is slippery).
    /// </summary>
    private void CalculateMovementDirection() {
        //Adding user input to moveDirection
        moveDirection = Vector3.zero;
        if (!cameraAnimation.isPanning) {
            if (walkThroughDoor) {
                moveDirection += Vector3.forward;
            }
            else {
                if (moveLeft)
                    moveDirection += Vector3.left;
                if (moveRight)
                    moveDirection += Vector3.right;

                baseInput = moveDirection;
            }
        }
        else {
            moveLeft = false;
            moveRight = false;
            moveDirection = Vector3.zero;
            baseInput = Vector3.zero;
        }

        //Calculating current acceleration
        if (moveDirection.magnitude > 0f) {
            acceleratedMoveSpeed += movement.acceleration;
            acceleratedMoveSpeed = Mathf.Clamp(acceleratedMoveSpeed, 0f, movement.moveSpeed);
        }
        else if (acceleratedMoveSpeed > movement.moveSpeed) {
            acceleratedMoveSpeed -= movement.acceleration;
        }
        else {
            acceleratedMoveSpeed -= movement.acceleration;
            acceleratedMoveSpeed = Mathf.Clamp(acceleratedMoveSpeed, 0f, movement.moveSpeed);
        }

        //If we're getting no input, check if we are touching a green cube (and therefore should keep sliding)
        if (moveDirection.magnitude == 0 && greenMultiplier > 1f) {
            moveDirection = characterController.velocity.normalized;
        }

        /* --------DEPRECATED???----------
         
        //Calculate the current acceleration.
        if (joystick.CurrentInput.magnitude > 0f) {
            acceleratedMoveSpeed += movement.acceleration;
            acceleratedMoveSpeed = Mathf.Clamp(acceleratedMoveSpeed, 0f, movement.moveSpeed);
        }
        else if (acceleratedMoveSpeed > movement.moveSpeed) {
            acceleratedMoveSpeed -= movement.acceleration;
        }
        else {
            acceleratedMoveSpeed -= movement.acceleration;
            acceleratedMoveSpeed = Mathf.Clamp(acceleratedMoveSpeed, 0f, movement.moveSpeed);
        }

        //Calculate the moveDirection from camera rotation + joystick input, and apply the accelerated moveSpeed.
        Quaternion rotation = new Quaternion(0f, mainCamera.rotation.y, 0f, mainCamera.rotation.w);
        moveDirection = rotation * joystick.CurrentInput;
        moveDirection.Normalize();

        //If we're getting no input, check if we are touching a green cube (and therefore should keep sliding)
        if (moveDirection.magnitude == 0 && greenMultiplier > 1f) {
            Vector3 normVelocity = characterController.velocity.normalized;
            moveDirection = rotation * new Vector3(normVelocity.x, 0, normVelocity.z);
            moveDirection.Normalize();
        }

    */

        moveDirection *= acceleratedMoveSpeed;

        //Apply gravity.
        if (applyGravity) {
            if (characterController.isGrounded) {
                acceleratedGravityPull = Physics.gravity.y * Time.deltaTime;
            }
            else {
                acceleratedGravityPull += Physics.gravity.y * Time.deltaTime;
            }

            moveDirection.y = acceleratedGravityPull;
        }
    }

    /// <summary>
    /// Calculates the lerped jump force (avoids the player instantly 'teleporting' upwards when touching a blue cube).
    /// </summary>
    private Vector3 LerpedJumpForce() {
        targetJumpForce = Vector3.zero;

        if (characterController.isGrounded) {
            currentJumpForce = Vector3.zero;
        }

        targetJumpForce = cubeInteraction.GetBlueForce();

        //If we are getting no target jump force, but our collider says we're touching a blue cube (which means we're standing on the very edge of a blue cube)
        if (currentJumpForce.magnitude == 0 && targetJumpForce.magnitude == 0 && blueCube) {
            //Calculating which direction we should jump in
            Vector3 direction = player.position - blueCube.transform.position;
            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) {
                if (direction.x < 0)
                    direction = Vector3.left;
                else
                    direction = Vector3.right;
            }
            else {
                if (direction.y < 0)
                    direction = Vector3.down;
                else
                    direction = Vector3.up;
            }

            targetJumpForce = cubeInteraction.GetCustomBlueForce(Vector3.up);
            cubeInteraction.BlueCubeBounceAnimation(blueCube);
        }
        blueCube = null;

        if (targetJumpForce.magnitude > 0)
            acceleratedGravityPull = Physics.gravity.y * Time.deltaTime;

        currentJumpForce = Vector3.Lerp(currentJumpForce, targetJumpForce, 0.1f);

        return currentJumpForce;
    }

    /// <summary>
    /// Rotates the character to face the direction he is currently moving.
    /// </summary>
    private void RotateCharacter() {
        if (walkThroughDoor) {
            player.LookAt(player.position + Vector3.forward);
        }
        if ((moveLeft && moveRight) || (!moveLeft && !moveRight))
            return;

        if (moveLeft)
            player.LookAt(player.position + Vector3.left);
        else if (moveRight)
            player.LookAt(player.position + Vector3.right);

        /*
        Quaternion rotation = new Quaternion(0f, mainCamera.rotation.y, 0f, mainCamera.rotation.w);
        Vector3 lookDir = rotation * joystick.CurrentInput;
        float angle = Vector3.Angle(player.forward, lookDir);

        if (angle < rotationThreshold)
            return;
        else if (Vector3.Cross(player.forward, lookDir).y < 0)
            angle *= -1f;

        //player.LookAt(player.position + new Vector3(moveDirection.x, 0, moveDirection.z));
        player.rotation *= Quaternion.AngleAxis(angle * movement.turnSpeed * Time.deltaTime, Vector3.up);
        */
    }

    public void EnemyPushback(Vector3 direction){
        direction.y = 0;
        enemyForce = direction.normalized * enemyPushbackForce;
    }

    public void StartJumpAnimation() {
        StartCoroutine(JumpAnimation());
    }

    IEnumerator JumpAnimation() {
        dustParticles.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        dustParticles.SetActive(false);
    }

    public void ActivateGreenDust() {
        foreach (Transform currentTransform in dustParticlesRun.transform) {
            print(currentTransform.gameObject.name);
            currentTransform.GetComponent<ParticleSystem>().Play();
        }
        //dustParticlesRun.GetComponentInChildren<ParticleSystem>().Play();
    }

    public void DeactivateGreenDust() {
        foreach (Transform currentTransform in dustParticlesRun.transform) {
            currentTransform.GetComponent<ParticleSystem>().Stop();
        }
        //dustParticlesRun.GetComponentInChildren<ParticleSystem>().Stop();
    }

    void OnControllerColliderHit(ControllerColliderHit hit) {
        if (hit.collider.tag == "Blue")
            blueCube = hit.gameObject;

        hitNormal = hit.normal;
    }
}
