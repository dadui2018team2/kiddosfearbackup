﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeIndicator : MonoBehaviour {

    [SerializeField] LineRenderer lineRendererBackground;
    [SerializeField] LineRenderer lineRendererFill;

    [SerializeField] Gradient redGradient;
    [SerializeField] Gradient greenGradient;
    [SerializeField] Gradient blueGradient;

    private GameObject player;
    private Shooting shootMechanic;

    private float maxLength;
    private Vector3[] positionsBackground = new Vector3[2];
    private Vector3[] positionsFill = new Vector3[2];

    private void Awake() {
        player = GameObject.FindWithTag("Player");
        shootMechanic = FindObjectOfType<Shooting>();

        maxLength = shootMechanic.maxShootingDistance;
    }

    private void FixedUpdate() {
        if (shootMechanic.shootDirection.magnitude > 0 && shootMechanic.colorToShoot != "") {
            if (lineRendererFill.gameObject.activeInHierarchy == false) {
                lineRendererBackground.gameObject.SetActive(true);
                lineRendererFill.gameObject.SetActive(true);

                switch (shootMechanic.colorToShoot) {
                    case "Red":
                        lineRendererFill.colorGradient = redGradient;
                        break;
                    case "Green":
                        lineRendererFill.colorGradient = greenGradient;
                        break;
                    case "Blue":
                        lineRendererFill.colorGradient = blueGradient;
                        break;
                    default:
                        lineRendererFill.colorGradient = redGradient;
                        break;
                }
            }

            transform.LookAt(transform.position + shootMechanic.shootDirection);

            positionsBackground[0] = player.transform.position;
            positionsBackground[1] = player.transform.position + shootMechanic.shootDirection * maxLength;
            lineRendererBackground.SetPositions(positionsBackground);

            positionsFill[0] = player.transform.position;
            positionsFill[1] = player.transform.position + shootMechanic.shootDirection * shootMechanic.currentShotMagnitude;
            lineRendererFill.SetPositions(positionsFill);

            lineRendererFill.endWidth = shootMechanic.currentShotMagnitude / maxLength;
        }
        else if (lineRendererFill.gameObject.activeInHierarchy) {
            positionsBackground[0] = Vector3.zero;
            positionsBackground[1] = Vector3.zero;
            positionsFill[0] = Vector3.zero;
            positionsFill[1] = Vector3.zero;

            lineRendererBackground.SetPositions(positionsBackground);
            lineRendererFill.SetPositions(positionsFill);

            lineRendererBackground.gameObject.SetActive(false);
            lineRendererFill.gameObject.SetActive(false);
        }
    }
}
