﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeInteraction : MonoBehaviour {

    public Transform lastKnownGroundCube;

    [Tooltip("How far away (in units) should be check for cubes?")]
    [SerializeField] float rayLength;

    private float detectionDistanceGround = 0.3f;
    private float detectionDistanceAir = 0.1f;
    private float detectionDistance;

    [SerializeField] int maximumGreenBoosts;

    [Space(10)]
    [SerializeField] float redPullStrength;
    [SerializeField] float blueBounceStrength;
    [SerializeField] float greenSpeedIncrease;

    private float currentGreenBoost = 1f;
    private Vector3 redNormals;
    private Vector3 blueNormals;
    private List<Collider> greenCubes = new List<Collider>();

    private int layerMask = 1 << 9;
    private float halfWidth;
    private float halfHeight;

    [HideInInspector]
    public float defaultSlopeLimit;
    private float currentSlopeLimit;
    [HideInInspector]
    public bool isOnWedge;

    CharacterController cc;
    PlayerMovement playerMovement;
    public bool greenSuccession;


    [Header("Variables for 'Bounce effect' on blue cube")]
    private bool canRunScaleCoroutine;
    public float scaleFactor;
    public AnimationCurve animationCurve;
    public float scaleMultiplier;

    private void Awake() {
        cc = GetComponent<CharacterController>();
        playerMovement = GetComponent<PlayerMovement>();
        halfWidth = cc.radius;
        halfHeight = cc.height / 2f;

        defaultSlopeLimit = cc.slopeLimit;
        currentSlopeLimit = defaultSlopeLimit;

        canRunScaleCoroutine = true;
    }

    public void DetectCubes() {
        if (cc.isGrounded)
            detectionDistance = detectionDistanceGround;
        else
            detectionDistance = detectionDistanceAir;

        greenSuccession = false;
        redNormals = Vector3.zero;
        blueNormals = Vector3.zero;

        //Checking for cubes front, back, left, right and up
        CheckCubeCollision(Vector3.forward, halfWidth, false);
        CheckCubeCollision(-Vector3.forward, halfWidth, false);
        CheckCubeCollision(Vector3.right, halfWidth, false);
        CheckCubeCollision(-Vector3.right, halfWidth, false);
        CheckCubeCollision(Vector3.up, halfHeight, false);

        //Checking for cubes down (which is the only direction that green cubes will be detected in)
        CheckCubeCollision(-Vector3.up, halfHeight, true);

        if ((greenCubes.Count > 0 && greenSuccession == false) || cc.velocity.magnitude < 0.01f)
            greenCubes.Clear();
    }
    
    public Vector3 GetBlueForce() {
        Vector3 direction = blueNormals.normalized;
        direction *= blueBounceStrength;

        if (direction.magnitude > 0) {
            playerMovement.StartJumpAnimation();
        }

        //if player walked into cube from side, add a bit of upwards bounce to keep him off the ground
        if (direction.magnitude > 0 && direction.y == 0) {
            direction.y += blueBounceStrength / 4f; 
        }

        return direction;
    }

    public Vector3 GetCustomBlueForce(Vector3 direction) {
        direction.Normalize();
        direction *= blueBounceStrength;

        if (direction.magnitude > 0) {
            playerMovement.StartJumpAnimation();
        }

        //if player walked into cube from side, add a bit of upwards bounce to keep him off the ground
        if (direction.magnitude > 0 && direction.y == 0) {
            direction.y += blueBounceStrength / 4f;
        }

        return direction;
    }

    public float GetGreenMultiplier() {
        float maximumBoost = 1f + greenSpeedIncrease * greenCubes.Count;

        if (currentGreenBoost < maximumBoost) {
            currentGreenBoost += 2f * Time.deltaTime;
            currentGreenBoost = Mathf.Clamp(currentGreenBoost, currentGreenBoost, maximumBoost);
        }
        else if (currentGreenBoost > maximumBoost) {
            //Decreasing the green speedboost (faster if on ground, slower if in air)
            if (cc.isGrounded) {
                currentGreenBoost -= 2f * Time.deltaTime;
            }
            else {
                currentGreenBoost -= Time.deltaTime;
            }
            currentGreenBoost = Mathf.Clamp(currentGreenBoost, 1f, currentGreenBoost);
        }

        //Playing the green speedboost sound
        if (greenSuccession) {
            float rtcpValue = cc.velocity.magnitude / 40f; //(currentGreenBoost - 1f) / ((1f + greenSpeedIncrease * maximumGreenBoosts) / 1f);
            rtcpValue = Mathf.Clamp(rtcpValue, 0f, 1f);
            AkSoundEngine.SetRTPCValue("Green_RTCP", rtcpValue);

            if (!AudioPlayer.isLoopingGreenBoost) {
                AkSoundEngine.PostEvent("GreenColor", gameObject);
                AudioPlayer.isLoopingGreenBoost = true;
            }
        }
        else if (AudioPlayer.isLoopingGreenBoost) {
            AkSoundEngine.PostEvent("Stop_GreenColor", gameObject);
            AudioPlayer.isLoopingGreenBoost = false;
            AkSoundEngine.SetRTPCValue("Green_RTCP", 0f);
        }

        return currentGreenBoost;
    }

    private void CheckCubeCollision(Vector3 direction, float detectionOffset, bool detectingDownwards) {
        Vector3 offset = direction * detectionOffset;
        RaycastHit hit;
        isOnWedge = false;

        //If we hit something...
        if (Physics.Raycast(transform.position, direction, out hit, rayLength, layerMask)) {
            if (detectingDownwards) {
                if (Vector3.Distance(transform.position + offset, hit.point) <= detectionDistance) {
                    //Saving the last cube/wedge we stood on (IF the cube is only in the plane we can move on, which is Z=0)
                    //lastKnownGroundCube = hit.collider.gameObject.transform;
                    string cubeType = hit.collider.gameObject.GetComponent<CubeColoring>().cubeType;
                    if (hit.collider.transform.position.z == 0 && cubeType == "Cube") {
                        lastKnownGroundCube = hit.collider.gameObject.transform;
                    }
                    else if (cubeType == "Wedge" && hit.collider.tag == "Wedge") {
                        isOnWedge = true;
                    }

                    //Changing the footstep sound to fit the ground type
                    switch (hit.collider.tag) {
                        case "Blue":
                            AudioPlayer.SetSwitch("Footsteptype", hit.collider.tag, gameObject);
                            break;
                        case "Red":
                        case "Green":
                        default:
                            AudioPlayer.SetSwitch("Footsteptype", "Normal", gameObject);
                            break;
                    }

                    //Ensuring the slope limit is decreased or reset based on whether we're on an uncolored wedge or not.
                    if (hit.collider.tag == "Wedge") {
                        currentSlopeLimit -= 1f;
                        currentSlopeLimit = Mathf.Clamp(currentSlopeLimit, 0f, 180f);
                        cc.slopeLimit = currentSlopeLimit;
                        return;
                    }
                    else {
                        currentSlopeLimit = defaultSlopeLimit;
                        cc.slopeLimit = currentSlopeLimit;
                    }
                }
            }

            switch (hit.collider.tag) {
                case "Green":
                    if (detectingDownwards && Vector3.Distance(transform.position + offset, hit.point) <= detectionDistance) {
                        greenSuccession = true;
                        if (!greenCubes.Contains(hit.collider) && greenCubes.Count < maximumGreenBoosts) {
                            greenCubes.Add(hit.collider);
                        }
                    }
                    break;
                case "Blue":
                    if (Vector3.Distance(transform.position + offset, hit.point) <= detectionDistance) {
                        blueNormals += hit.normal;
                        BlueCubeBounceAnimation(hit.collider.gameObject);
                    }
                    break;
                default:
                    //Debug.LogError("Hit " + hit.collider.gameObject.name + " with tag " + hit.collider.tag + ".");
                    break;
            }
        }
    }

    public void BlueCubeBounceAnimation(GameObject cube) {
        StartCoroutine(ScaleCube(cube));
    }

    private IEnumerator ScaleCube(GameObject cube) {
        if (canRunScaleCoroutine) {
            canRunScaleCoroutine = false;
            Vector3 cubeScale = cube.transform.localScale;

            float scaleTime = 0;

            yield return new WaitForSeconds(.05f);
            
            while (scaleTime < 1) {
                
                scaleTime += Time.deltaTime * scaleMultiplier;

                cube.transform.localScale = cubeScale * animationCurve.Evaluate(scaleTime);

                yield return new WaitForEndOfFrame();
            }
            /*
            scaleTime = 1;
            while (cube.transform.localScale.x > 0) {
                scaleTime -= Time.deltaTime;

                cube.transform.localScale = Vector3.Lerp(cubeScale, newCubeScale, animationCurve.Evaluate(scaleTime));

                yield return new WaitForEndOfFrame();
            }*/
            
            canRunScaleCoroutine = true;
        }
    }
}
