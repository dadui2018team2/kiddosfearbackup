﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBlink : MonoBehaviour {

	[Tooltip("Material used by player.")]
	public Material playerMaterial;
	[Tooltip("Time between blinks.")]
	public float timeBetween = 0.2f;

	private float blinkDuration = 5f;
	private IEnumerator blinking;
	//private cakeslice.Outline outline;

	private void Awake() {
		//outline = GetComponentInChildren<cakeslice.Outline>();
	}

	/// <summary>
    /// Starts the blinking with the given duration.
    /// </summary>
	public void StartBlink(float duration){
		blinkDuration = duration;
		Blink();
	}

	/// <summary>
    /// Stop blinking if player is blinking.
    /// </summary>
	public void StopBlink(){
		if (blinking != null){
			StopCoroutine(blinking);
		}
		Color c = playerMaterial.color;
		c.a = 1;
		playerMaterial.color = c;
		//outline.enabled = true;
	}

	private void Blink(){
		if (blinking != null){
			StopCoroutine(blinking);
			blinking = Blinking();
			StartCoroutine(blinking);
		}
		else {
			blinking = Blinking();
			StartCoroutine(blinking);
		}
	}

	private IEnumerator Blinking(){
		float remainingTime = blinkDuration;
		Color c = playerMaterial.color;
		while(remainingTime > 0){
			remainingTime -= timeBetween;
			if (c.a != 1){
				c.a = 1;
				//outline.enabled = true;
			}
			else {
				c.a = 0;
				//outline.enabled = false;
			}
			playerMaterial.color = c;
			yield return new WaitForSeconds(timeBetween);
		}
		c = playerMaterial.color;
		c.a = 1;
		playerMaterial.color = c;
		//outline.enabled = true;
	}

	private void OnDisable() {
		Color c = playerMaterial.color;
		c.a = 1;
		playerMaterial.color = c;
		//outline.enabled = true;
	}
}
