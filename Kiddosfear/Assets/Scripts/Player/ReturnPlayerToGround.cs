﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnPlayerToGround : MonoBehaviour {

    [Header("Player recovery speed")]
    [SerializeField] private float speedModifier;

    [Header("Private variables changed runtime")]
    [SerializeField] private GameObject player;
    [SerializeField] private Transform lastKnownCube;
    [SerializeField] private float tParam;
    [SerializeField] private bool coroutineAllowed;
    [SerializeField] private Vector3[] route;

    private CubeInteraction cubeInteraction;

    private void Start() {
        player = gameObject;
        coroutineAllowed = true;
        int[] test = new int[5];
        route = new Vector3[4];

        cubeInteraction = GetComponent<CubeInteraction>();
    }

    public void ReturnPlayer() {
        if (coroutineAllowed) {
            StartCoroutine(MovePlayer());
        }
    }

    IEnumerator MovePlayer() {
        GetComponent<PlayerMovement>().enabled = false;
        lastKnownCube = GetComponent<CubeInteraction>().lastKnownGroundCube;

        CalculateRoutePoints();
        
        while (tParam < 1) {
            tParam += Time.deltaTime * speedModifier;

            player.transform.position = Mathf.Pow(1 - tParam, 3) * route[0] +
                3 * Mathf.Pow(1 - tParam, 2) * tParam * route[1] +
                3 * (1 - tParam) * Mathf.Pow(tParam, 2) * route[2] + Mathf.Pow(tParam, 3) * route[3];

            yield return new WaitForEndOfFrame();
        }
        tParam = 0f;
        coroutineAllowed = true;
        GetComponent<PlayerMovement>().enabled = true;

    }

    private void CalculateRoutePoints() {

        route[0] = player.transform.position;
        route[1] = new Vector3(player.transform.position.x, player.transform.position.y + 10, player.transform.position.z);
        route[2] = new Vector3(lastKnownCube.position.x, lastKnownCube.position.y + 10, lastKnownCube.position.z);
        route[3] = new Vector3(lastKnownCube.position.x, lastKnownCube.position.y + 5, lastKnownCube.position.z);
    }

    private void OnTriggerEnter(Collider other) {
        if (cubeInteraction.lastKnownGroundCube != null) {
            if (other.gameObject.tag == "KillZone") {
                if(cubeInteraction.lastKnownGroundCube.tag == "Red") {
                    cubeInteraction.lastKnownGroundCube.GetComponent<CubeColoring>().ChangeColor("");
                }
                ReturnPlayer();
            }
        }
    }
}
