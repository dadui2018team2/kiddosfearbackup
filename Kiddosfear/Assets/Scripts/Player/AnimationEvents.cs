﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour {

    ActionAnimator actionAnimator;

    private void Awake() {
        actionAnimator = FindObjectOfType<ActionAnimator>();
    }

    public void PlayNormalFootStep() {
        if (actionAnimator.LocomotionLayerHidden())
            return;

        AudioPlayer.PlayEvent("KiddoFootsteps", gameObject, 1f);
    }

    public void PlaySpecialFootStep() {
        AudioPlayer.PlayEvent("KiddoFootsteps", gameObject, 1f);
    }

    public void PlayLanding() {
        AudioPlayer.PlayEvent("KiddoLanding", gameObject, 1f);
    }
}
