﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static int blocksPainted = 0;

    public static GameObject Player { get; private set; }
    private GameObject _player;

    private static GameManager instance;
    private static Vector3 spawnPoint;
    private static CameraAnimation camAnim;

    [Header("General settings")]
	public bool _movePlayerToStart = true;
	public Transform _spawnPoint;

    public string _nextLevel;
    private static string nextLevel;

    [Header("Level name settings")]
	public Text _levelNameText;
    public float _fadeTime = 1f;
	public float _visibleTime = 1f;
	private Coroutine levelNameAnimation;

	[Header("Debug")]
	public bool _skipToPlay = false;
    [Tooltip("Set the height the player should spawn from when entering from the main menu")]
    public int fromMainMenuHeight;

    public static GameState CurrentGameState;
    public enum GameState{
		Begin,
		ShowLevelName,
		Play,
		End,
	}

	private void Awake() {
        //Getting all the necessary references to be used
        instance = this;

        _player = GameObject.FindWithTag("Player");
		Player = _player;
        nextLevel = _nextLevel;

        if (_spawnPoint != null)
            spawnPoint = _spawnPoint.position;
		else
            spawnPoint = Player.transform.position;

        camAnim = Camera.main.GetComponent<CameraAnimation>();

        //Setting the gamestate to start with
        CurrentGameState = GameState.Begin;
	}

    private void Update() {
		RunGameState();
	}

	///<summary>This will check the current game state and run the appropriate piece of code</summary>
	private void RunGameState(){
		switch(CurrentGameState){
			case GameState.Begin:
                // Level is loaded, move player to start point and reset amount of blocks painted
                blocksPainted = 0;
				if (_movePlayerToStart){
                    if (MainMenuSettings.previousScene == "MainMenu") {
                        spawnPoint.y += fromMainMenuHeight;
                    }
                    MovePlayerToStart();
				}

				if (_skipToPlay){
					CurrentGameState = GameState.Play;
				}
				else {
                    CurrentGameState = GameState.ShowLevelName;
				}
			    break;

            case GameState.ShowLevelName:
                //Start the fading/panning, which automatically ends out changing the GameState to Play.
                if (levelNameAnimation == null)
                    levelNameAnimation = StartCoroutine(AnimateLevelName());
                break;

			case GameState.Play:
                // Player can move and complete the level
                break;

			case GameState.End:
                NextScene();
			break;
		}
	}

	///<summary>Will give player the position of startpoint</summary>
	public static void MovePlayerToStart(){
        Player.transform.position = spawnPoint;
	}

	///<summary>Method to load next scene</summary>
	private static void NextScene() {
        string currentLevelCheckpoint = "Level" + SceneManager.GetActiveScene().buildIndex + "Checkpoint";
        PlayerPrefs.SetInt(currentLevelCheckpoint, 0);

		FindObjectOfType<UIGameManager>().LoadScene(nextLevel);
	}

	///<summary>Method to initiate player death</summary>
	public static void PlayerDeath(){
		MovePlayerToStart();
	}

    public void ResetDoorsPlayerPref() {
        PlayerPrefs.SetInt("CompletedLevels", 0);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private IEnumerator AnimateLevelName() {
        GameObject exitDoor = GameObject.FindWithTag("ExitDoor");

        //Begin the pan, but don't move yet
        camAnim.Pan(exitDoor.transform, Player.transform, 0f, 1f, true);
        camAnim.pausePan = true;

        //Fade in from black
        camAnim.BeginFade(1f, 0f, 0.01f);
        while (camAnim.isFading)
            yield return new WaitForEndOfFrame();

        //Fade the level name in/out
        _levelNameText.gameObject.SetActive(true);
        _levelNameText.color = new Color(_levelNameText.color.r, _levelNameText.color.g, _levelNameText.color.b, 0f);
        bool fadeOut = false;

        while (true) {
            //Decide the fade value for this frame
            float fadeDelta = Time.deltaTime / _fadeTime;
            if (fadeOut)
                fadeDelta = -fadeDelta;

            //Apply the alpha change to the text
            Color newColor = _levelNameText.color;
            newColor.a += fadeDelta;
            _levelNameText.color = newColor;

            //Check whether we reached one or the other end of the fade
            if (fadeOut == false && newColor.a >= 1) {
                //We have faded in, now wait for some time before starting to fade out
                fadeOut = true;
                yield return new WaitForSeconds(_visibleTime);
            }
            else if (fadeOut && newColor.a <= 0) {
                //Start panning the camera and exit/end this coroutine
                _levelNameText.gameObject.SetActive(false);
                camAnim.pausePan = false;

                CurrentGameState = GameState.Play;
                levelNameAnimation = null;
                break;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    /*

    private IEnumerator QuoteFadeRoutine(){
		bool reverse = false;
		while (true) {
			float fadeDelta = Time.deltaTime / _fadeTime;
			if (reverse) fadeDelta = -fadeDelta;
			Color c = _text.color;
			c.a += fadeDelta;
			_text.color = c;

			if (reverse && c.a <= 0) {
				if (_levelImage != null){
					levelImageFadeRoutine = LevelImageFadeRoutine();
					StartCoroutine(levelImageFadeRoutine);
				}
				else CurrentGameState = GameState.UIFadeIn;
				StopCoroutine(levelNameFading);
			}
			else if (!reverse && c.a >= 1) {
				reverse = true;
				yield return new WaitForSeconds(_visibleTime);
			}
			yield return null;
		}
	}

	private IEnumerator LevelImageFadeRoutine(){
		while(true) {
			float fadeDelta = Time.deltaTime / _levelImageFadeTime;
			Color c = _levelImage.color;
			c.a -= fadeDelta;
			_levelImage.color = c;

			if (_levelImage.color.a <= 0){
				_levelImage.gameObject.SetActive(false);
				CurrentGameState = GameState.CameraStartAnimation;
				StopCoroutine(levelImageFadeRoutine);
			}
			yield return null;
		}
	}
    */
}
