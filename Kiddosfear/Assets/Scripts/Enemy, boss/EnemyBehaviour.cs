﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

	/*
	Script for Enemy. Can move to patrol points and will always look in the direction of movement.
	*/

	[Header("Enemy Attributes")]
	public float _moveSpeed = 1f;
	public float _rotateSpeed = 90f;
	public float _size = 1f;
	public float _startSize = 0;
	public float _sizeSpeed = 1f;
	public float _respawnTime = 1f;

	[Header("Enemy Patrol")]
	public GameObject _startPoint;
	public List<GameObject> _patrolPoints;
	public float _pointReachDistance = 0f;

	[Header("Sounds")]
	[Range(0, 1)]
	public int _enemyType = 0;
	public float _reactionTime = 1f;
	
	private GameObject startPoint;
	private List<GameObject> patrolPoints;

	private bool dead = false;

	private SkinnedMeshRenderer skinnedMeshRenderer;
	private Animator animator;

    private bool canPlayPatrolSound;

	private void Awake() {
		startPoint = _startPoint;
		patrolPoints = _patrolPoints;
		skinnedMeshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
		animator = GetComponentInChildren<Animator>();
	}

	private void Start() {
		StartPatrol();
        canPlayPatrolSound = false;
        PatrolSoundTimer();
	}
	
	// Update is called once per frame
	void Update () {
		if (!dead){
			MoveEnemy();
		}
	}

	private void StartPatrol(){
        if (canPlayPatrolSound)
            PlaySound("MonsterType", "Monster" + _enemyType, "MonsterSound", "Monster");

		//AudioPlayer.PlayEventWithSwitch("MonsterType", "MonsterInteraction", "MonsterSound", gameObject, 3);
		dead = false;
		patrolPoints = new List<GameObject>(_patrolPoints);
		transform.position = startPoint.transform.position;
		transform.LookAt(patrolPoints[0].transform);

		transform.localScale = Vector3.one * _startSize;
	}

	private void Die(){
		// Some animation and smoke, and delay when the monster is moved.
		PlaySound("MonsterType", "Monster" + _enemyType, "MonsterDies", "Monster");
		//AkSoundEngine.SetSwitch("MonsterType", "Monster1", gameObject);
		//AudioPlayer.PlayEventWithSwitch("MonsterType", "MonsterInteraction", "MonsterDies", gameObject, 3);
		dead = true;
		transform.position = startPoint.transform.position;
		transform.localScale = Vector3.one * _startSize;
		Invoke("StartPatrol", _respawnTime);
	}

	///<summary>Will move the enemy to next point. Upon reaching, move point back in the list.</summary>
	private void MoveEnemy(){

		if (animator){
			animator.SetBool("Move", true);
		}

		// Scaling
		if (transform.localScale.x < _size){
			float newScale = Mathf.Lerp(_startSize, _size, transform.localScale.x + Time.deltaTime / _sizeSpeed);
			transform.localScale = Vector3.one * newScale;
		}

		// Position
		float distanceToPoint = Vector3.Distance(transform.position, patrolPoints[0].transform.position);
		Vector3 moveDir = (patrolPoints[0].transform.position - transform.position).normalized;
		if (distanceToPoint < _moveSpeed * Time.deltaTime) moveDir *= distanceToPoint;
		else moveDir *= _moveSpeed * Time.deltaTime;
        transform.position += moveDir;

		// Rotation
		if (moveDir.magnitude > 0){
            //transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(moveDir, Vector3.up), _rotateSpeed * Time.deltaTime);
            transform.LookAt(patrolPoints[0].transform);
		}

		// Check destination
		if (distanceToPoint <= _pointReachDistance){
			NextPoint();
		}
	}

	///<summary>Move current point back in the list.</summary>
	private void NextPoint(){
		patrolPoints.Add(patrolPoints[0]);
		patrolPoints.RemoveAt(0);
	}

	private void OnTriggerEnter(Collider other) {
        Projectile projectile = other.GetComponent<Projectile>();
		if (projectile != null){
            projectile.Stop();
			Die();
		}
        if (other.gameObject.tag == "Player") {
            PlayerMovement playerMovement = other.GetComponent<PlayerMovement>();

            EnemyHit();

            Vector3 contactNormal = other.transform.position - transform.position;
            contactNormal.Normalize();
            contactNormal.z = 0;
            
            playerMovement.EnemyPushback(contactNormal);

        }
    }

	public void EnemyHit(){
		PlaySound("MonsterType", "Monster" + _enemyType, "MonsterHitsKiddo", "Monster");
		//AkSoundEngine.SetSwitch("MonsterType", "Monster1", gameObject);
		//AudioPlayer.PlayEventWithSwitch("MonsterType", "MonsterInteraction", "MonsterHitsKiddo", gameObject, 3);
		Invoke("EnemyReaction", _reactionTime);

		if (animator){
			animator.SetTrigger("React");
		}
	}

	private void EnemyReaction(){
		PlaySound("MonsterType", "Monster" + _enemyType, "MonsterReactionToHittingKiddo", "Monster");
		//AkSoundEngine.SetSwitch("MonsterType", "Monster1", gameObject);
        //AudioPlayer.PlayEventWithSwitch("MonsterType", "MonsterInteraction", "MonsterReactionToHittingKiddo", gameObject, 3);

		if (animator){
			animator.SetTrigger("React");
		}
    }

	private void PlaySound(string _event, string _type, string _interaction, string _bank){
		AkSoundEngine.SetSwitch("MonsterType", _type, gameObject);
		AkSoundEngine.SetSwitch("MonsterInteraction", _interaction, gameObject);
		AudioPlayer.LoadBank(_bank);
		AkSoundEngine.PostEvent("MonsterType", gameObject);
		AudioPlayer.UnloadBank(_bank, 3f);
	}

    IEnumerator PatrolSoundTimer() {
        yield return new WaitForSeconds(1f);
        canPlayPatrolSound = true;
    }
}
