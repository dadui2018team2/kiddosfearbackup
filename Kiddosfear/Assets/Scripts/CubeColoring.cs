﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeColoring : MonoBehaviour {
    [Tooltip("Is this is a Cube or a Wedge?")]
    public string cubeType;
    private bool animIsPlaying = false;
    private Animator anim;

    [Tooltip("If left blank, the cube will just grab its texture at scene start.")]
    [SerializeField] Texture originalTexture;
    [Tooltip("If left blank, the cube will just grab its color at scene start.")]
    [SerializeField] Color originalColor = Color.magenta;
    private Material mat;
    private Collider col;

    private void Awake() {
        col = GetComponent<Collider>();
        //mat = GetComponentInChildren<MeshRenderer>().material ? GetComponentInChildren<MeshRenderer>().material : GetComponentInChildren<SkinnedMeshRenderer>().material;

        if (GetComponentInChildren<MeshRenderer>() == null) {
            mat = GetComponentInChildren<SkinnedMeshRenderer>().material;
            anim = GetComponent<Animator>();
            anim.speed = 0;
        }
        else {
            mat = GetComponentInChildren<MeshRenderer>().material;
        }
        
        //originalTexture = mat.mainTexture;
        if(originalTexture == null)
            originalTexture = mat.GetTexture("_Albedo");
        if (originalColor == Color.magenta)
            originalColor = mat.GetColor("_Tint");

    }

    public void ChangeColor(string newColor) {
        if(GetComponentInChildren<MeshRenderer>() == null) {
            whaleFlying flyingWhale = GetComponent<whaleFlying>();

            //Setting the new color
            mat.color = ConvertStringToColor(newColor);
            mat.SetColor("_Tint", ConvertStringToColor(newColor));

            if (!animIsPlaying && newColor != "") {
                flyingWhale.StartOrbit();
                anim.speed = 1;
                animIsPlaying = true;
                
            }
            if (newColor == "") {
                flyingWhale.StopOrbit();
                anim.speed = 0;
                animIsPlaying = false;
            }

            //Setting the new tag, texture and smoothness
            if (newColor == "") {
                gameObject.tag = cubeType;
                mat.mainTexture = originalTexture;
                mat.SetFloat("_Glossiness", 0f);

                //mat.SetTexture("_Albedo", originalTexture);
            }
            else {
                gameObject.tag = newColor;
                mat.mainTexture = null;
                mat.SetFloat("_Glossiness", 0.4f);

               // mat.SetTexture("_Albedo", null);
            }
        }
        else {
            AnimateArt animArt = GetComponent<AnimateArt>();

            if (animArt != null)
                animArt.DetermineArtType(newColor);

            //Setting the new color
            mat.color = ConvertStringToColor(newColor);
            mat.SetColor("_Tint", ConvertStringToColor(newColor));

            //Setting the new tag, texture and smoothness
            if (newColor == "") {
                gameObject.tag = cubeType;
                mat.mainTexture = originalTexture;
                mat.SetFloat("_Glossiness", 0f);

                mat.SetTexture("_Albedo", originalTexture);
            }
            else {
                gameObject.tag = newColor;
                mat.mainTexture = null;
                mat.SetFloat("_Glossiness", 0.4f);

                mat.SetTexture("_Albedo", null);
            }

            //Modifying the shader type and the collider in case the cube is colored red
            if (newColor == "Red") {
                col.isTrigger = true;
                mat.SetFloat("_Alpha", 0.4f);
            }
            else {
                col.isTrigger = false;
                mat.SetFloat("_Alpha", 1f);
            }
        }
    }

    private Color ConvertStringToColor(string color) {
        switch (color) {
            case "":
                return originalColor;
            case "Red":
                return new Color(0.6f, 0f, 0f);
            case "Green":
                return new Color(0f, 0.6f, 0f);
            case "Blue":
                return new Color(0f, 0f, 0.6f);
            default:
                return Color.magenta;
        }
    }
}
