﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VacuumTarget : MonoBehaviour {

	private Material material;
	private VacuumPoint vacuumPoint;

	private void Awake() {
		material = GetComponentInChildren<MeshRenderer>().sharedMaterial;
		vacuumPoint = FindObjectOfType<VacuumPoint>();

		if (!vacuumPoint) this.enabled = false;

		material.SetFloat("_UseVacuum", 1f);
		Color c = Random.ColorHSV(0f, 1f, 0.5f, 1f, 0.9f, 1f, 1f, 1f);
		material.SetColor("_Tint", c);
	}
	
	void Update () {
		material.SetFloat("_UseVacuum", vacuumPoint.vacuumOn);
		material.SetFloat("_VacuumStart", vacuumPoint.vacuumStart);
		material.SetFloat("_VacuumEnd", vacuumPoint.vacuumEnd);
		material.SetVector("_Vacuum", vacuumPoint.transform.position);
	}
}
