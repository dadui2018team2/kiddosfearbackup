﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSounds : MonoBehaviour {

	public void PlayBossSpawning() {
        AudioPlayer.LoadBank("BossSound");
		AkSoundEngine.PostEvent("BossSpawning", gameObject);
        AudioPlayer.UnloadBank("BossSound", 1f);
    }

	public void PlayBossSound() {
        AudioPlayer.PlayEvent("BossSound", gameObject, 1f);
    }
}
