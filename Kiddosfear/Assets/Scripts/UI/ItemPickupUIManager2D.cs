﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPickupUIManager2D : MonoBehaviour {

    
    [Header("pickup counter")]
    [Tooltip("Is set automatically upon start")]
    public Text pickupCounter;

    private void Start() {

        pickupCounter = GetComponentInChildren<Text>();
            }
    /// <summary>
    /// Updates the pickup counter textelemt whenever the player picks up a pickup
    /// </summary>
    /// <param name="counter"></param>
    public void UpdatePickup(int counter) {
        pickupCounter.text = counter.ToString();
    }
    }

