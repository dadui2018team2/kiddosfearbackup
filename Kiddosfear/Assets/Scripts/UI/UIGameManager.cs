﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class UIGameManager : MonoBehaviour {

    private PlayerMovement playerMovement;
    private TouchPaint touchPaint;
    private GameManager gameManager;

    private int pickupsCollected = 0;

    [SerializeField] Color buttonSelectionColor;
    [SerializeField] Animator panelAnimator;

    [Header("Canvas fading")]
    [SerializeField] Text[] textToFade;
    [SerializeField] Image[] imagesToFade;
    [SerializeField] Button[] buttonsToDisable;
    [SerializeField] EventTrigger[] eventTriggersToDisable;

    [Header("Color selection")]
    [Tooltip("The size of the button when it is selected. default is (1, 1)")]
    [SerializeField] Vector3 selectedButtonSizeChange;
    [SerializeField] Button redButton;
    [SerializeField] Button greenButton;
    [SerializeField] Button blueButton;
    [SerializeField] Button eraseButton;
    private Vector3 originalButtonSize;

    [Header("Pickup GUI")]
    [SerializeField] Text pickupCounter;
    [SerializeField] Text pickupCounterShadow;
    [SerializeField] Image[] pickups = new Image[3];

    [Header("Settings menu")]
    [SerializeField] GameObject levelHubButton;
    [SerializeField] Image danishColoredFlag;
    [SerializeField] Image englishColoredFlag;
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider soundSlider;

    private void Awake() {
        playerMovement = FindObjectOfType<PlayerMovement>();
        touchPaint = FindObjectOfType<TouchPaint>();
        gameManager = FindObjectOfType<GameManager>();
        originalButtonSize = redButton.image.rectTransform.localScale;

        CheckColorProgression();
        CheckAudioSettings();

        if (SceneManager.GetActiveScene().name == "Level0_2D")
            levelHubButton.SetActive(false);

        if (PlayerPrefs.HasKey("CurrentColor"))
            SelectColor(PlayerPrefs.GetString("CurrentColor"));
    }

    /// <summary>
    /// Makes Kiddo move towards the left.
    /// </summary>
    public void MoveLeft(bool move) {
        playerMovement.moveLeft = move;
    }

    /// <summary>
    /// makes Kiddo move towards the right.
    /// </summary>
    public void MoveRight(bool move) {
        playerMovement.moveRight = move;
    }

    /// <summary>
    /// Selects the color given as string parameter.
    /// </summary>
    /// <param name="color">Red, Green, Blue or empty for erasing.</param>
    public void SelectColor(string color) {
        //TODO
        switch (color) {
            case "Red":
            case "Green":
            case "Blue":
            case "":
            case "None":
                UpdatePaintButtonSelection(color);
                touchPaint.SelectColor(color);
                break;
            default:
                Debug.LogError("UI attempted to select invalid color: " + color);
                break;
        }
    }

    /// <summary>
    /// Checks whether any colors have previously been picked up, and enables the relevant buttons.
    /// </summary>
    private void CheckColorProgression() {
        if (!PlayerPrefs.HasKey("RedEnabled"))
            PlayerPrefs.SetInt("RedEnabled", 0);
        if (!PlayerPrefs.HasKey("GreenEnabled"))
            PlayerPrefs.SetInt("GreenEnabled", 0);
        if (!PlayerPrefs.HasKey("BlueEnabled"))
            PlayerPrefs.SetInt("BlueEnabled", 0);

        bool colorIsUnlocked = false;

        if (PlayerPrefs.GetInt("RedEnabled") == 1) {
            EnableColor("Red");
            colorIsUnlocked = true;
        }
        if (PlayerPrefs.GetInt("GreenEnabled") == 1) {
            EnableColor("Green");
            colorIsUnlocked = true;
        }
        if (PlayerPrefs.GetInt("BlueEnabled") == 1) {
            EnableColor("Blue");
            colorIsUnlocked = true;
        }

        if (colorIsUnlocked)
            EnableColor("");
    }

    public void CheckLanguageSelection() {
        if (PlayerPrefs.GetInt("lang") == 0)
            englishColoredFlag.gameObject.SetActive(true);
        else
            danishColoredFlag.gameObject.SetActive(true);
    }

    private void CheckAudioSettings() {
        if (!PlayerPrefs.HasKey("Music_Volume"))
            PlayerPrefs.SetFloat("Music_Volume", 50f);

        if (!PlayerPrefs.HasKey("Sound_Volume"))
            PlayerPrefs.SetFloat("Sound_Volume", 50f);

        musicSlider.value = PlayerPrefs.GetFloat("Music_Volume");
        soundSlider.value = PlayerPrefs.GetFloat("Sound_Volume");

        AkSoundEngine.SetRTPCValue("Music_Volume", musicSlider.value);
        AkSoundEngine.SetRTPCValue("Sound_Volume", soundSlider.value);
    }

    /// <summary>
    /// Activates the color button for the color given as string parameter.
    /// </summary>
    /// <param name="color">Red, Green, Blue or empty for erasing.</param>
    public void EnableColor(string color) {
        switch (color) {
            case "Red":
                redButton.gameObject.SetActive(true);
                PlayerPrefs.SetInt("RedEnabled", 1);
                break;
            case "Green":
                greenButton.gameObject.SetActive(true);
                PlayerPrefs.SetInt("GreenEnabled", 1);
                break;
            case "Blue":
                blueButton.gameObject.SetActive(true);
                PlayerPrefs.SetInt("BlueEnabled", 1);
                break;
            case "":
                eraseButton.gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Resizes the button when a color is selected
    /// </summary>
    /// <param name="color"></param>
    public void UpdatePaintButtonSelection(string color) {
        redButton.image.rectTransform.localScale = originalButtonSize;
        greenButton.image.rectTransform.localScale = originalButtonSize;
        blueButton.image.rectTransform.localScale = originalButtonSize;
        eraseButton.image.rectTransform.localScale = originalButtonSize;

        redButton.image.color = Color.white;
        greenButton.image.color = Color.white;
        blueButton.image.color = Color.white;
        float eraseDefault = 120f / 255f;
        eraseButton.image.color = new Color(eraseDefault, eraseDefault, eraseDefault);

        switch (color) {
            case "Red":
                redButton.image.rectTransform.localScale += selectedButtonSizeChange;
                redButton.image.color = Color.red;
                break;
            case "Green":
                greenButton.image.rectTransform.localScale += selectedButtonSizeChange;
                greenButton.image.color = Color.green;
                break;
            case "Blue":
                blueButton.image.rectTransform.localScale += selectedButtonSizeChange;
                blueButton.image.color = Color.blue;
                break;
            case "":
                eraseButton.image.rectTransform.localScale += selectedButtonSizeChange;
                eraseButton.image.color = Color.white;
                break;
            case "None":
                break;
            default:
                Debug.LogError("UI attempted to resize invalid color: " + color);
                break;
        }
    }

    public void IncreasePickupCounter() {
        pickupsCollected++;
        string newText = pickupsCollected.ToString() + " / 3";

        pickupCounter.text = newText;
        pickupCounterShadow.text = newText;
    }

    /// <summary>
    /// Updating the pickup GUI element.
    /// </summary>
    /// <param name="pickupsCollected">If true, this pickup has been collected.</param>
    public void UpdatePickupCounter(bool[] pickupsCollected) {
        for (int i = 0; i < pickups.Length; i++) {
            if (pickupsCollected[i]) {
                pickups[i].gameObject.SetActive(true);
            }
            else {
                pickups[i].gameObject.SetActive(false);
            }
        }
        AudioPlayer.PlayEvent("KeyUI", gameObject, 3f);
    }

    public void RestartLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    public void OpenPanel(GameObject panel) {
        panel.SetActive(true);

        if (panel.name.ToLower().Contains("guioverlay")) {
            AudioPlayer.SetState("StateOfGame", "Menu");
        }
    }

    public void ClosePanel(GameObject panel) {
        panel.SetActive(false);

        if (panel.name.ToLower().Contains("guioverlay")) {
            AudioPlayer.SetState("StateOfGame", "InGame");
        }
    }

    public void FadeIn(GameObject panel) {
        panel.SetActive(true);
        Image img = panel.GetComponent<Image>();
        StartCoroutine(FadeImage(img, 0f, 0.5f, false));
    }

    public void FadeOut(GameObject panel) {
        Image img = panel.GetComponent<Image>();
        StartCoroutine(FadeImage(img, 0.5f, 0f, true));
    }

    public void PlaySoundOnClick(string soundToPlay) {
        AudioPlayer.SetSwitch("UI", soundToPlay, gameObject);
        AkSoundEngine.PostEvent("UI", gameObject);
    }

    public void ColorTextSelect(Text text) {
        text.color = buttonSelectionColor;
    }

    public void ColorTextUnselect(Text text) {
        text.color = Color.white;
    }

    /// <summary>
    /// Show the GUI? If true, it fades in.
    /// </summary>
    /// <param name="show"></param>
    public void ShowGUI(bool show) {
        if (show) {
            StartCoroutine(FadeGUIIn(FindObjectOfType<CameraAnimation>().fadeStep));
        }
        else {
            for (int i = 0; i < textToFade.Length; i++) {
                Color color = textToFade[i].color;
                color.a = 0f;
                textToFade[i].color = color;
            }
            for (int i = 0; i < imagesToFade.Length; i++) {
                imagesToFade[i].color = Color.clear;
            }
            for (int i = 0; i < buttonsToDisable.Length; i++) {
                buttonsToDisable[i].interactable = false;
            }
            for (int i = 0; i < eventTriggersToDisable.Length; i++) {
                eventTriggersToDisable[i].enabled = false;
            }
        }
    }

    private IEnumerator FadeGUIIn(float fadeValue) {
        float currentAlpha = 0f;

        for (int i = 0; i < buttonsToDisable.Length; i++) {
            buttonsToDisable[i].interactable = true;
        }
        for (int i = 0; i < eventTriggersToDisable.Length; i++) {
            eventTriggersToDisable[i].enabled = true;
        }

        while (currentAlpha < 1f) {
            currentAlpha += fadeValue;
            currentAlpha = Mathf.Clamp(currentAlpha, 0f, 1f);
            for (int i = 0; i < imagesToFade.Length; i++) {
                imagesToFade[i].color = new Color(1f, 1f, 1f, currentAlpha);
            }
            for (int i = 0; i < textToFade.Length; i++) {
                Color color = textToFade[i].color;
                color.a = currentAlpha;
                textToFade[i].color = color;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator FadeImage(Image img, float startAlpha, float endAlpha, bool setInactivateAtEnd) {
        float fadeValue = 0f;

        float currentAlpha = startAlpha;
        Color currentColor = img.color;
        currentColor.a = currentAlpha;

        while (currentAlpha != endAlpha) {
            fadeValue += Time.unscaledDeltaTime * 2f; //fading in half a second
            currentAlpha = Mathf.Lerp(startAlpha, endAlpha, fadeValue);
            currentColor.a = currentAlpha;

            img.color = currentColor;
            yield return new WaitForEndOfFrame();
        }

        if (setInactivateAtEnd) {
            while (panelAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.98f) {
                yield return new WaitForEndOfFrame();
            }
            img.gameObject.SetActive(false);
        }
    }

    public void PauseGame(bool pause) {
        if (pause)
            Time.timeScale = 0f;
        else
            Time.timeScale = 1f;
    }

    public void PlayPanelAnimation(string animName) {
        panelAnimator.Play(animName);
    }

    public void SetMusicVolume() {
        AkSoundEngine.SetRTPCValue("Music_Volume", (musicSlider.value));
        PlayerPrefs.SetFloat("Music_Volume", musicSlider.value);
    }

    public void SetSoundVolume() {
        AkSoundEngine.SetRTPCValue("Sound_Volume", (soundSlider.value));
        PlayerPrefs.SetFloat("Sound_Volume", soundSlider.value);
    }

    #region Developer tools
    /// <summary>
    /// Resets and disables the color buttons and the color pickup progression.
    /// </summary>
    public void ResetColors() {
        PlayerPrefs.SetInt("RedEnabled", 0);
        PlayerPrefs.SetInt("GreenEnabled", 0);
        PlayerPrefs.SetInt("BlueEnabled", 0);

        redButton.gameObject.SetActive(false);
        greenButton.gameObject.SetActive(false);
        blueButton.gameObject.SetActive(false);
        eraseButton.gameObject.SetActive(false);

        PlayerPrefs.SetString("CurrentColor", "None");
        FindObjectOfType<TouchPaint>().SelectColor("None");
    }

    /// <summary>
    /// Unlocks all colors
    /// </summary>
    public void UnlockAllColors() {
        PlayerPrefs.SetInt("RedEnabled", 1);
        PlayerPrefs.SetInt("GreenEnabled", 1);
        PlayerPrefs.SetInt("BlueEnabled", 1);

        redButton.gameObject.SetActive(true);
        greenButton.gameObject.SetActive(true);
        blueButton.gameObject.SetActive(true);
        eraseButton.gameObject.SetActive(true);
    }

    public void UnlockLevels() {
        PlayerPrefs.SetInt("CompletedLevels", 10);

        InGameDoors exitDoor = GameObject.FindWithTag("ExitDoor").GetComponent<InGameDoors>();
        exitDoor.AddKeyPart(0);
        exitDoor.AddKeyPart(1);
        exitDoor.AddKeyPart(2);
    }

    public void ResetLevels() {
        gameManager.ResetDoorsPlayerPref();
    }

    //The Sliders assume that we range 0-1, if range from wwise has to be 0-100, simply multiply the slider.value by 100
    #endregion
}
