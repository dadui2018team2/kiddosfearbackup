﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class UIMenuManager : MonoBehaviour {

    [SerializeField] Color buttonSelectionColor;

    [Header("Menu roots")]
    [SerializeField] GameObject settings;
    [SerializeField] GameObject quit;

    [Header("Canvas fading")]
    [SerializeField] Image blackFadeImage;
    [SerializeField] GameObject titleScreen;
    [SerializeField] float secondsWithTitleScreen;
    //[SerializeField] Text[] textToFade;
    //[SerializeField] Image[] imagesToFade;
    //[SerializeField] Button[] buttonsToDisable;
    //[SerializeField] EventTrigger[] eventTriggersToDisable;

    [Header("Settings menu")]
    [SerializeField] Image danishColoredFlag;
    [SerializeField] Image englishColoredFlag;
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider soundSlider;

    private void Awake() {
        //Fading in from black
        blackFadeImage.color = Color.black;
        StartCoroutine(FadeInFromBlack());

        //Checking audio settings
        CheckAudioSettings();
        CheckLanguageSelection();

        if (MainMenuSettings.firstLaunch) {
            titleScreen.SetActive(true);
            StartCoroutine(FadeAfterSeconds(titleScreen, secondsWithTitleScreen));
        }
        else {
            titleScreen.SetActive(false);
        }
    }

    private void CheckAudioSettings() {
        if (!PlayerPrefs.HasKey("Music_Volume"))
            PlayerPrefs.SetFloat("Music_Volume", 50f);

        if (!PlayerPrefs.HasKey("Sound_Volume"))
            PlayerPrefs.SetFloat("Sound_Volume", 50f);

        musicSlider.value = PlayerPrefs.GetFloat("Music_Volume");
        soundSlider.value = PlayerPrefs.GetFloat("Sound_Volume");

        AkSoundEngine.SetRTPCValue("Music_Volume", musicSlider.value);
        AkSoundEngine.SetRTPCValue("Sound_Volume", soundSlider.value);
    }

    public void CheckLanguageSelection() {
        if (PlayerPrefs.GetInt("lang") == 0)
            englishColoredFlag.gameObject.SetActive(true);
        else
            danishColoredFlag.gameObject.SetActive(true);
    }

    public void SetMusicVolume() {
        AkSoundEngine.SetRTPCValue("Music_Volume", (musicSlider.value));
        PlayerPrefs.SetFloat("Music_Volume", musicSlider.value);
    }

    public void SetSoundVolume() {
        AkSoundEngine.SetRTPCValue("Sound_Volume", (soundSlider.value));
        PlayerPrefs.SetFloat("Sound_Volume", soundSlider.value);
    }

    public void LoadScene(string sceneName) {
        MainMenuSettings.EndOfFirstLaunch();
        StartCoroutine(LoadSceneAfterWait(sceneName));
    }

    private IEnumerator LoadSceneAfterWait(string sceneName) {
        yield return new WaitForSeconds(2f);

        blackFadeImage.gameObject.SetActive(true);
        float currentAlpha = 0f;
        float fadeValue = 0.01f;

        while (currentAlpha < 1f) {
            currentAlpha += fadeValue;
            currentAlpha = Mathf.Clamp(currentAlpha, 0f, 1f);

            Color col = blackFadeImage.color;
            col.a = currentAlpha;
            blackFadeImage.color = col;

            yield return new WaitForEndOfFrame();
        }

        AsyncOperation newScene = SceneManager.LoadSceneAsync(sceneName);
    }

    public void QuitApplication() {
        Application.Quit();
    }

    public void OpenPanel(GameObject panel) {
        panel.SetActive(true);
    }

    public void ClosePanel(GameObject panel) {
        panel.SetActive(false);
    }

    public void FadeIn(GameObject panel) {
        panel.SetActive(true);
        Image img = panel.GetComponent<Image>();
        StartCoroutine(FadeImage(img, 0f, 0.5f, false));
    }

    public void FadeOut(GameObject panel) {
        Image img = panel.GetComponent<Image>();
        StartCoroutine(FadeImage(img, 0.5f, 0f, true));
    }

    public void FadePanel(GameObject panel) {
        StartCoroutine(FadePanelWithChildren(panel, 0.01f));
    }

    #region UI animations and sounds
    public void PlaySoundOnClick(string soundToPlay) {
        AudioPlayer.SetSwitch("UI", soundToPlay, gameObject);
        AkSoundEngine.PostEvent("UI", gameObject);
    }

    public void ColorTextSelect(Text text) {
        text.color = buttonSelectionColor;
    }

    public void ColorTextUnselect(Text text) {
        text.color = Color.white;
    }

    public void OpenSettingsPanel(bool open) {
        if (open) {
            settings.GetComponent<Animator>().Play("InFromRight");
            
        }
        else
            settings.GetComponent<Animator>().Play("OutToRight");
    }

    public void OpenQuitPanel(bool open) {
        if (open) {
            quit.GetComponent<Animator>().Play("InFromLeft");

        }
        else
            quit.GetComponent<Animator>().Play("OutToLeft");
    }
    #endregion

    #region Fading coroutines
    private IEnumerator FadeInFromBlack() {
        float currentAlpha = 1f;
        float fadeValue = 0.01f;

        while (currentAlpha > 0f) {
            currentAlpha -= fadeValue;
            currentAlpha = Mathf.Clamp(currentAlpha, 0f, 1f);

            Color col = blackFadeImage.color;
            col.a = currentAlpha;
            blackFadeImage.color = col;

            yield return new WaitForEndOfFrame();
        }

        blackFadeImage.gameObject.SetActive(false);
    }

    private IEnumerator FadeAfterSeconds(GameObject panel, float seconds) {
        yield return new WaitForSeconds(seconds);

        Animator anim = panel.GetComponent<Animator>();
        anim.Play("SlideUp");
        while (anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.75f) {
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(FadePanelWithChildren(panel, 0.008f));
    }

    private IEnumerator FadePanelWithChildren(GameObject panel, float fadeValue) {
        Button[] buttons = panel.GetComponentsInChildren<Button>();
        for (int i = 0; i < buttons.Length; i++) {
            buttons[i].enabled = false;
        }

        Text[] texts = panel.GetComponentsInChildren<Text>();
        Image[] images = panel.GetComponentsInChildren<Image>();

        float currentAlpha = 1f;

        while (currentAlpha > 0f) {
            currentAlpha -= fadeValue;
            currentAlpha = Mathf.Clamp(currentAlpha, 0f, 1f);

            for (int i = 0; i < texts.Length; i++) {
                Color col = texts[i].color;
                col.a = currentAlpha;
                texts[i].color = col;
            }

            for (int i = 0; i < images.Length; i++) {
                Color col = images[i].color;
                col.a = currentAlpha;
                images[i].color = col;
            }

            yield return new WaitForEndOfFrame();
        }

        panel.gameObject.SetActive(false);
    }

    private IEnumerator FadeImage(Image img, float startAlpha, float endAlpha, bool setInactivateAtEnd) {
        float fadeValue = 0f;

        float currentAlpha = startAlpha;
        Color currentColor = img.color;
        currentColor.a = currentAlpha;

        while (currentAlpha != endAlpha) {
            fadeValue += Time.unscaledDeltaTime * 2f; //fading in half a second
            currentAlpha = Mathf.Lerp(startAlpha, endAlpha, fadeValue);
            currentColor.a = currentAlpha;

            img.color = currentColor;
            yield return new WaitForEndOfFrame();
        }

        if (setInactivateAtEnd) {
            Animator anim = GetComponentInChildren<Animator>();
            while (anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.98f) {
                yield return new WaitForEndOfFrame();
            }
            img.gameObject.SetActive(false);
        }
    }

    #endregion
}
