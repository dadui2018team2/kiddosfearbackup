﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColourPaletteAnimator : MonoBehaviour {

    public Animator blueanimator;
    public Animator redanimator;
    public Animator greenanimator;

    private void Start() {
        if (PlayerPrefs.HasKey("CurrentColor")) {
            switch (PlayerPrefs.GetString("CurrentColor")) {
                case "Red":
                    RedAnimator(true);
                    break;
                case "Green":
                    GreenAnimator(true);
                    break;
                case "Blue":
                    BlueAnimator(true);
                    break;
                default:
                    break;
            }
        }
    }

    public void BlueAnimator(bool state)
    {
        blueanimator.SetBool("Open", state);
       
    }

    public void RedAnimator(bool state)
    {
        redanimator.SetBool("Open", state);

    }

    public void GreenAnimator(bool state)
    {
        greenanimator.SetBool("Open", state);

    }
}
