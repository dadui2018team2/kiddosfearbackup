﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleScreenFadeIn : MonoBehaviour {

    private Image image;

    private void Awake() {
        image = GetComponent<Image>();
    }

    private void Start() {
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut() {
        yield return new WaitForSeconds(1f);
        Color newColor = image.color;
        while (image.color.a > 0) {
            newColor = image.color;
            newColor.a -= Time.deltaTime;
            image.color = newColor;
            yield return new WaitForEndOfFrame();
        }
        gameObject.SetActive(false);
    }
}
