﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageSettings : MonoBehaviour
{

    /*
	This script is for changing the shown language in the game.
	Add to text/sprite and then give both english and danish text/sprite.

	Based on an int:
		0 = English
		1 = Dansih
	
	 */

    public string english;
    public string danish;

    //public Sprite englishSprite;
    //public Sprite danishSprite;

    private string langKey = "lang";
    private int currentLang;

    private Text textComponent;
    //private Image imageComponent;

    private void Awake() {
        danish = danish.Replace("\\n", "\n");
        english = english.Replace("\\n", "\n");
    }

    void Start()
    {
        textComponent = GetComponent<Text>();
        //imageComponent = GetComponent<Image>();
        UpdateThis();
    }

    public void ChangeLanguageENG()
    {
        //PlayerPrefs.SetInt("VoiceOverLanguage", 0);
        currentLang = PlayerPrefs.GetInt(langKey, 0);
        if (currentLang == 1)
        {
            PlayerPrefs.SetInt(langKey, 0);
        }
        LanguageObjectsUpdate();
    }

    public void ChangeLanguageDK()
    {
        //PlayerPrefs.SetInt("VoiceOverLanguage", 1);
        currentLang = PlayerPrefs.GetInt(langKey, 0);
        if (currentLang == 0)
        {
            PlayerPrefs.SetInt(langKey, 1);
        }
        LanguageObjectsUpdate();
    }

    public void LanguageObjectsUpdate()
    {
        LanguageSettings[] lsArray = FindObjectsOfType<LanguageSettings>();
        foreach (LanguageSettings ls in lsArray)
        {
            ls.UpdateThis();
        }

        //Change active speechbubble text
        TextMeshLanguageSettings textMesh = FindObjectOfType<TextMeshLanguageSettings>();
        if (textMesh)
            textMesh.LanguageObjectsUpdate();
    }

    private void OnEnable() {
        UpdateThis(); 
    }

    private void UpdateThis()
    {
        currentLang = PlayerPrefs.GetInt(langKey, 0);
        if (textComponent != null)
        {
            if (currentLang == 0)
            {
                textComponent.text = english;
            }
            else
            {
                textComponent.text = danish;
            }
        }
        /*if (imageComponent != null && englishSprite != null)
        {
            if (currentLang == 0)
            {
                imageComponent.sprite = englishSprite;
            }
            else
            {
                imageComponent.sprite = danishSprite;
            }
        }*/
    }
}
