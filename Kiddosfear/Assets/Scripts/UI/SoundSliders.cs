﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSliders : MonoBehaviour {

    [Header("Sound and music elements")]
    public Slider musicSlider;
    public Slider soundSlider;



    void Start () {
		
	}


    public void SetMusicVolume()
    {
        AkSoundEngine.SetRTPCValue("Music_Volume", (musicSlider.value));
        PlayerPrefs.SetFloat("Music_Volume", musicSlider.value);
    }

    public void SetSoundVolume()
    {
        AkSoundEngine.SetRTPCValue("Sound_Volume", (soundSlider.value));
        PlayerPrefs.SetFloat("Sound_Volume", soundSlider.value);
    }
}
