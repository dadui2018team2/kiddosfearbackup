﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour {

    private static AudioPlayer instance;

    public static bool isLoopingRedPull;
    public static bool isLoopingGreenBoost;

    private void Awake() {
        instance = this;
        LoadBank("Loops");
    }

    private void OnDestroy() {
        int bankUnload;
        AkSoundEngine.UnloadBank("Loops", (System.IntPtr)0, out bankUnload);
    }

    /// <summary>
    /// Loads a soundbank from Wwise.
    /// </summary>
    /// <param name="bankId">The name of the bank to load.</param>
    public static void LoadBank(string bankId) {
        uint bankLoad;
        AkSoundEngine.LoadBank(bankId, -1, out bankLoad);
    }

    /// <summary>
    /// Unloads a soundbank from Wwise.
    /// </summary>
    /// <param name="bankId">The name of the bank to unload.</param>
    /// /// <param name="unloadWaitTime">Seconds to wait before unloading.</param>
    public static void UnloadBank(string bankId, float unloadWaitTime) {
        instance.StartCoroutine(UnloadBankAfterSeconds(bankId, unloadWaitTime));
    }

    /// <summary>
    /// Plays an event.
    /// </summary>
    /// <param name="soundId">The name of the bank/event. OBS: If these are not the same, use AudioPlayer.LoadBank, AkSoundEngine.PostEvent and AudioPlayer.UnloadBank instead.</param>
    /// <param name="obj">The object from where the sound is playing.</param>
    /// <param name="unloadWaitTime">How long to wait before unloading the soundbank (if unsure, use "3f").</param>
    public static void PlayEvent(string soundId, GameObject obj, float unloadWaitTime) {
        uint bankLoad;
        AkSoundEngine.LoadBank(soundId, -1, out bankLoad);
        AkSoundEngine.PostEvent(soundId, obj);

        if(instance != null)
            instance.StartCoroutine(UnloadBankAfterSeconds(soundId, unloadWaitTime));
    }

    /// <summary>
    /// Plays an event AFTER switching.
    /// </summary>
    /// <param name="soundId">The name of the event.</param>
    /// <param name="switchGroupId">The name of the switch group to change.</param>
    /// <param name="switchId">The name of the switch.</param>
    /// <param name="obj">The object from where the sound is playing.</param>
    /// <param name="unloadWaitTime">How long to wait before unloading the soundbank (if unsure, use "3f").</param>
    public static void PlayEventWithSwitch(string soundId, string switchGroupId, string switchId, GameObject obj, float unloadWaitTime) {
        SetSwitch(switchGroupId, switchId, obj);
        PlayEvent(soundId, obj, unloadWaitTime);
    }

    /// <summary>
    /// Changes a switch group.
    /// </summary>
    /// <param name="swichGroupId">The name of the switch group to change.</param>
    /// <param name="swichId">The name of the switch.</param>
    public static void SetSwitch(string switchGroupId, string switchId, GameObject obj) {
        AkSoundEngine.SetSwitch(switchGroupId, switchId, obj);
    }

    /// <summary>
    /// Changes the state of a state group.
    /// </summary>
    /// <param name="stateGroupId">The name of the state group to change state in.</param>
    /// <param name="stateId">The name of the state to switch to.</param>
    public static void SetState(string stateGroupId, string stateId) {
        AkSoundEngine.SetState(stateGroupId, stateId);
    }

    private static IEnumerator UnloadBankAfterSeconds(string soundId, float waitTime) {
        int bankUnload;
        yield return new WaitForSeconds(waitTime);
        AkSoundEngine.UnloadBank(soundId, (System.IntPtr)0, out bankUnload);
    }
}
