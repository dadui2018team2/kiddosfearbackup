﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InGameDoors : MonoBehaviour {
    
    Animator animator;
    public Animator lockAnimator;

    public bool exitDoor;

    public bool[] keyParts;
    BoxCollider boxCollider;

    public string sceneToGoTo;

    private bool waitingForPan = false;

    

    private void Awake() {
        animator = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider>();
        if (!PlayerPrefs.HasKey("CompletedLevels")) {
            PlayerPrefs.SetInt("CompletedLevels", 0);
        }
    }

    private void Start() {
        if (exitDoor) {
            ActivateDoor();


            // ---*** KEEP THIS CODE IN CASE THEY WANT IT CHANGED BACK TO HAVING THE DOORS OPEN AFTER COMPLETING IT ONCE ***---
            /*if(PlayerPrefs.HasKey("CompletedLevels")) {
                int currentLevel = GetSceneValue();
                if (currentLevel <= PlayerPrefs.GetInt("CompletedLevels")) {
                    print("current level: " + currentLevel + ". " + "PlayerPref: " + PlayerPrefs.GetInt("CompletedLevel"));
                    OpenDoor();
                }
            }*/
        }
        else {
            if (MainMenuSettings.previousScene == "MainMenu")
                gameObject.SetActive(false);
            else
                StartCoroutine(DeactivateEntranceDoor());

        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.R)) {
            ResetDoorsPlayerPref();
        }
        if (Input.GetKeyDown(KeyCode.U)) {
            if(exitDoor) {
                UnlockDoor();
            }
        }
    }

    private void ResetDoorsPlayerPref() {
            PlayerPrefs.SetInt("CompletedLevels", 0);
    }

    IEnumerator DeactivateEntranceDoor() {
        yield return new WaitForSeconds(1f);
        DeactivateDoor();
    }

    public void AddKeyPart(int keyPart) {
        keyParts[keyPart] = true;
        FindObjectOfType<UIGameManager>().UpdatePickupCounter(keyParts);
        if (CheckDoor()) {
            UnlockDoor();
            AudioPlayer.PlayEvent("DoorIsLockedUp", gameObject, 3f);
        }
    }

    private bool CheckDoor() {
        foreach(bool part in keyParts) {
            if (part == false) return false;
        }
        return true;
    }

    private void UnlockDoor() {
        lockAnimator.Play("Unlock");
        Invoke("OpenDoor", 3f);
    }

    /// <summary>
    /// Activates the door.
    /// </summary>
    public void ActivateDoor() {
        animator.Play("DoorActivate");
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorAppear", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Plays activate sound.
    /// </summary>
    public void PlayActivateSound() {
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorAppear", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Opens the door (Should only be called after activating!)
    /// </summary>
    public void OpenDoor() {
        animator.Play("OpenDoor");
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorOpen", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Plays open sound.
    /// </summary>
    public void PlayOpenSound() {
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorOpen", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Closes the door.
    /// </summary>
    public void DeactivateDoor() {
        animator.Play("DoorDeactivate");
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorClose", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    private int GetSceneValue() {
        string currentSceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        currentSceneName = currentSceneName.Substring(5, 1);
        int currentLevel = (int)char.GetNumericValue(currentSceneName[0]);
        if (currentLevel == -1) Debug.Log("I couldn't find a number at 6th position of the level, instead it was: " + currentSceneName);
        return currentLevel;
    }

    /*IEnumerator PlayerExitDoor() {
        PlayerMovement playerMovement = FindObjectOfType<PlayerMovement>();
        playerMovement.walkThroughDoor = true;
        while (Vector3.Distance(transform.position, playerMovement.gameObject.transform.position) > 2f) {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(.25f);
        playerMovement.walkThroughDoor = false;
        playerMovement.enabled = false;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            StartCoroutine(PlayerExitDoor());

            CameraAnimation camAnim = FindObjectOfType<CameraAnimation>();
            try {
                Transform panPos = GameObject.FindWithTag("LevelExitPanPos").transform;

                Debug.Log("panPos != null: " + panPos);
                UIGameManager uiControl = FindObjectOfType<UIGameManager>();
                uiControl.MoveLeft(false);
                uiControl.MoveRight(false);
                if (!waitingForPan)
                    camAnim.Pan(GameManager.Player.transform, panPos, 1f, 0f);

                if (!waitingForPan) {
                    StartCoroutine(WaitForPan(camAnim));
                }
            }
            catch(NullReferenceException ex) {
                Debug.Log("NullReference: couldnt find pan position. did you remember to add gameobject with the tag 'LevelExitPanPos'");
            }

            //UIGameManager uiMenuManager = FindObjectOfType<UIMenuManager>();
            //uiGameManager.LoadScene("LevelSelect");
            if (PlayerPrefs.HasKey("CompletedLevels")) {
                int currentLevel = GetSceneValue();

                if (currentLevel > PlayerPrefs.GetInt("CompletedLevels")) {
                    PlayerPrefs.SetInt("CompletedLevels", currentLevel);
                }
            }
        }
    }

    IEnumerator WaitForPan(CameraAnimation camAnim) {
        waitingForPan = true;
        int counter = 0;
        while (camAnim.isPanning || counter < 100) {
            yield return new WaitForEndOfFrame();
            Debug.Log("Is panning: " + camAnim.isPanning);
            counter++;
        }

        counter = 0;

        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneToGoTo);
        waitingForPan = false;
    }*/

}

    /*[Header("Updated in awake")]
    public LevelDoorManager levelDoorManager;
    public Animator animator;

    [Header("Door variables (do not touch)")]
    public bool isActive;
    public bool isOpen;
    public GameObject otherDoor;
    


    private void Awake() {
        levelDoorManager = transform.parent.GetComponent<LevelDoorManager>();
        animator = GetComponent<Animator>();

    }

    /// <summary>
    /// Checks if the current door is active. If it isn't, it will activate.
    /// </summary>
    public void AttemptActivateDoor() {
        if (!isActive) {
            if (levelDoorManager.DoorIsNewCheckpoint(gameObject)) {
                levelDoorManager.ActivateDoors(this);
                int doorValue = (int)char.GetNumericValue(gameObject.name[gameObject.name.Length - 1]);
                levelDoorManager.SaveDoorCheckpoint(doorValue);
            }
        }
    }
    
    

    

    /// <summary>
    /// Sets which door to teleport to, when entering the door.
    /// </summary>
    /// <param name="door"></param>
    public void SetOtherDoor(GameObject door) {
        otherDoor = door;
    }

    

    /// <summary>
    /// Invokes private or public function in the script.
    /// </summary>
    /// <param name="functionName">Name of the function to run</param>
    /// <param name="time">Delay before running the function</param>
    public void InvokeClassFunction(string functionName, float time) {
        Invoke(functionName, time);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            DoorInteraction otherDoorInteraction = otherDoor.GetComponent<DoorInteraction>();

            if (otherDoor == null) Debug.Log("Can't find 'otherDoor' in DoorInteraction.cs. Current door: " + gameObject.name);

            otherDoorInteraction.OpenDoor();
            other.transform.position = otherDoorInteraction.gameObject.transform.position + (otherDoorInteraction.gameObject.transform.forward * 2);
            AudioPlayer.SetSwitch("TeleDoor", "TeleDoorKiddoWalksThrough", otherDoor.gameObject);
            AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
            otherDoorInteraction.Invoke("CloseDoor", 1f);
        }
    }
}



    */