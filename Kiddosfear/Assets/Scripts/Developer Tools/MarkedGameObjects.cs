﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[ExecuteInEditMode]
public class MarkedGameObjects : MonoBehaviour {
#if UNITY_EDITOR
    /*
	This is for replacing selected objects.
	 */

    public GameObject[] prefabs;
	public int prefabIndex;

	///<summary>Used to update the list of prefabs</summary>
	public void UpdatePrefabs(string path){
		string[] assetPaths = AssetDatabase.GetAllAssetPaths();
		
		List<GameObject> prefabList = new List<GameObject>();
		 for (int i = 0; i < assetPaths.Length; i++){
			 if (assetPaths[i].Contains(path)){
				GameObject _asset = (GameObject)AssetDatabase.LoadAssetAtPath(assetPaths[i], typeof(GameObject));
				try {
					string name = _asset.name;
					prefabList.Add(_asset);
				}
				catch (NullReferenceException e){
				}
			 }
		 }
		Debug.Log("Found " + assetPaths.Length + " assets, of which " + prefabList.Count + " are prefab(s).");
		prefabs = prefabList.ToArray();
	}

	///<summary>Replaces selected objects with prefabs</summary>
	public void ReplaceObjects() {
		try {
			GameObject[] objects = Selection.gameObjects;
			string message = "";
			for (int i = 0; i < objects.Length; i++){
				message += objects[i].name + "\n";
				GameObject newObject = Instantiate(prefabs[prefabIndex], objects[i].transform.position, objects[i].transform.rotation);
				DestroyImmediate(objects[i], false);
				objects[i] = newObject;
			}
			message += "New object: " + prefabs[prefabIndex].name;
			Debug.Log(message);
			Selection.objects = objects;
		}
		
		catch (NullReferenceException e){
			Debug.Log("Null reference exception!");
		}
	}
    #endif
}
