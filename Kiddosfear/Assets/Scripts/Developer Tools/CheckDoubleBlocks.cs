﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckDoubleBlocks : MonoBehaviour {

    public bool checkOverlaps;

    public List<GameObject> overlaps = new List<GameObject>();

    private CubeColoring[] cubes = new CubeColoring[0];

    private void OnValidate() {
        if (checkOverlaps) {
            CheckOverlaps();
            checkOverlaps = false;
        }

        
    }

    private void CheckOverlaps() {
        Debug.Log("CHECKING FOR BLOCKS ON TOP OF EACH OTHER...");

        cubes = FindObjectsOfType<CubeColoring>();

        List<string> printMessages = new List<string>();
        overlaps = new List<GameObject>();

        for (int i = 0; i < cubes.Length - 1; i++) {
            int checkIndex = i + 1;

            while (checkIndex < cubes.Length) {
                if (Vector3.Distance(cubes[i].transform.position, cubes[checkIndex].transform.position) < 1) {
                    string result = "OVERLAPPING BLOCKS: " + cubes[i].name + " and " + cubes[checkIndex].name;
                    printMessages.Add(result);

                    overlaps.Add(cubes[i].gameObject);
                    overlaps.Add(cubes[checkIndex].gameObject);
                }

                checkIndex++;
            }
        }

        if (printMessages.Count < 1)
            Debug.Log("FINISHED CHECK. NO OVERLAPS FOUND.");

        Debug.LogError("FINISHED CHECK. FOUND " + printMessages.Count + " OVERLAPS.");
        for (int i = 0; i < printMessages.Count; i++) {
            Debug.LogError(printMessages[i]);
        }
    }
}
