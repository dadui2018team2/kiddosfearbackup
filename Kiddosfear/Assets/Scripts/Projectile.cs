﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    [Header("General settings")]
    [Tooltip("How many seconds should pass before the projectile despawns if no cubes have been hit?")]
    [SerializeField] float lifeSpanInSeconds;
    [Tooltip("How fast should the projectile initially move?")]
    [SerializeField] float projectileSpeed;
    [SerializeField] AnimationCurve forwardMovementCurve;
    [SerializeField] AnimationCurve fallCurve;

    [Header("Projectile materials")]
    [SerializeField] Material red;
    [SerializeField] Material green;
    [SerializeField] Material blue;
    private MeshRenderer rend;

    [Header("ParticleSystem")]
    [Tooltip("The range which rays are send out to detect surfaces")]
    [SerializeField] float DetectionRange;
    [SerializeField] GameObject ps;
    private ParticleSystemRenderer psRend;

    private Vector3 startingPosition;
    private Vector3 currentPosition;
    private Vector3 targetPosition;

    private string color;
    private Material material;

    private int layerInteractableCubes = 1 << 9;

    Coroutine move;

    private void Awake() {
        rend = GetComponent<MeshRenderer>();
        psRend = ps.GetComponent<ParticleSystemRenderer>();
    }

    /// <summary>
    /// Starting projectile movement towards a target position with a specified color/material to put on cubes if they are hit.
    /// </summary>
    public void InitializeProjectile(Vector3 startingPosition, Vector3 targetPosition, string color, Material material) {
        this.startingPosition = startingPosition;
        currentPosition = startingPosition;
        this.targetPosition = targetPosition;
        this.color = color;
        this.material = material;

        transform.position = this.startingPosition;
        ChangeOwnMaterial(this.color);

        if (move == null)
            move = StartCoroutine("Move");
    }

    /// <summary>
    /// Moving the projectile each frame.
    /// </summary>
    IEnumerator Move() {
        float lifeLeft = lifeSpanInSeconds;
        float curveEvaluationStep = 0f;
        float movementEvaluation = 0f;
        float fallEvaluation = 0f;
        float acceleratedGravity = 0f;

        Vector3 floorPoint = targetPosition;
        floorPoint.y -= 0.75f; //37.5% of the player height (or 3/8). Ask Ea if you need to know why this number... But please don't ask.

        while (true) {
            curveEvaluationStep += projectileSpeed * Time.deltaTime;
            movementEvaluation = forwardMovementCurve.Evaluate(curveEvaluationStep);
            fallEvaluation = fallCurve.Evaluate(curveEvaluationStep);

            currentPosition = Vector3.Lerp(startingPosition, targetPosition, movementEvaluation);
            currentPosition.y = Vector3.Lerp(targetPosition, floorPoint, fallEvaluation).y;

            if(fallEvaluation >= 1) {
                acceleratedGravity += Physics.gravity.y * fallEvaluation * Time.deltaTime;
                currentPosition.y += acceleratedGravity;
            }

            transform.LookAt(currentPosition, Vector3.up);
            transform.position = currentPosition;

            lifeLeft -= Time.deltaTime;
            if (lifeLeft <= 0)
                Stop();

            yield return new WaitForEndOfFrame();
        } 
    }

    /// <summary>
    /// If the projectile hit an interactable cube, color the cube, color the particle system and stop this projectile.
    /// </summary>
    private void OnTriggerEnter(Collider other) {
        if ((layerInteractableCubes & 1 << other.gameObject.layer) == 1 << other.gameObject.layer){ //checking whether the thing we hit is on the "InteractableCubes" layer
            //If the cube/wedge was not already painted, increase the counter in the game manager.
            if (other.gameObject.tag == "Cube" || other.gameObject.tag == "Wedge") {
                GameManager.blocksPainted += 1;
                switch (GameManager.blocksPainted) {
                    case 5:
                        AudioPlayer.SetState("Music_Color", "Colors5");
                        break;
                    case 10:
                        AudioPlayer.SetState("Music_Color", "Colors10");
                        break;
                    case 15:
                        AudioPlayer.SetState("Music_Color", "Colors15");
                        break;
                    case 20:
                        AudioPlayer.SetState("Music_Color", "Colors20");
                        break;
                    default:
                        if (GameManager.blocksPainted < 5) {
                            AudioPlayer.SetState("Music_Color", "None");
                        }
                        break;
                }
            }

            //Give the cube the new tag and material
            other.gameObject.tag = color;
            other.GetComponentInChildren<MeshRenderer>().material = material;

            Vector3 hitColliderPoint = other.ClosestPointOnBounds(transform.position);
            Vector3 normalDirection = DetectSurface(transform.position, hitColliderPoint);
            ChangeParticleColor(color);
            //Instantiate the particlesystem where the projectile hit the surface
            GameObject particleSystem = Instantiate(ps, other.ClosestPoint(transform.position), Quaternion.LookRotation(normalDirection));

            AudioPlayer.PlayEventWithSwitch("PaintingHitsSurface", "PaintingSurface", color, other.gameObject, 3f);

            Stop();
        }
        else if(other.tag == "Enemy" || other.tag == "Boss") {
            Vector3 hitColliderPoint = other.ClosestPointOnBounds(transform.position);
            Vector3 normalDirection = DetectSurface(transform.position, hitColliderPoint);
            GameObject particleSystem = Instantiate(ps, other.ClosestPoint(transform.position), Quaternion.LookRotation(normalDirection));

            AudioPlayer.PlayEvent("KiddoPaintMonster", gameObject, 3f);
        }
    }
   
    /// <summary>
    /// Stopping the Move-coroutine and setting this projectile as inactive.
    /// </summary>
    public void Stop() {
        //Stop moving this projectile
        if (move != null) {
            StopCoroutine(move);
            move = null;
        }

        AkSoundEngine.PostEvent("Stop_ColorFlyingThroughAir", gameObject);

        //Disable this projectile
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Changing the color (material) of the projectile.
    /// </summary>
    private void ChangeOwnMaterial(string color) {
        switch (color) {
            case "Red":
                rend.material = red;
                gameObject.GetComponentInChildren<ParticleSystemRenderer>().material.color = Color.red;
                break;
            case "Green":
                rend.material = green;
                gameObject.GetComponentInChildren<ParticleSystemRenderer>().material.color = Color.green;
                break;
            case "Blue":
                rend.material = blue;
                gameObject.GetComponentInChildren<ParticleSystemRenderer>().material.color = Color.blue;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Changeing the material on the particles in the particle system
    /// </summary>
    /// <param name="color"></param>
    private void ChangeParticleColor(string color) {
        switch (color) {
            case "Red":
                psRend.material = red;
                break;
            case "Green":
                psRend.material = green;
                break;
            case "Blue":
                psRend.material = blue;
                break;
            default:
                Debug.Log("This color doesn't exist");
                break;
        }
    }

    /// <summary>
    /// Cast ray in all 6 directions to determine which surface was hit, then return the normal of the surface hit.
    /// </summary>
    /// <param name="startPos"></param>
    /// <returns></returns>
    private Vector3 DetectSurface(Vector3 startPos, Vector3 colliderHitPoint) {

        Vector3 particleDirection = startPos - colliderHitPoint;
        if (particleDirection == Vector3.zero)
            particleDirection = Vector3.up;

        return particleDirection;

    }
    
}
