﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateArt : MonoBehaviour {

    public string artType;

    private Animator animatorController;
    private bool statueTurned = false;

	// Use this for initialization
	void Start () {
		
	}

    /// <summary>
    /// Determines which art type this object is based on a string in the inspector. Calls function based on art type
    /// </summary>
    /// <param name="color">Specify which color should activate this animation. If all colors write any color</param>
    public void DetermineArtType(string color) {
        switch (artType) {
            case "Ball":
                if (color == "Blue")
                    JumpingBall();
                break;
            case "Statue":
                if (color == "")
                    TurnStatueBack();
                else
                    TurnStatue();
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Plays animation for rolling the ball off the edge in level 1
    /// </summary>
    private void JumpingBall() {
        animatorController = GetComponent<Animator>();
        animatorController.Play("BouncyBounce");

        StartCoroutine(WaitForAnim("BouncyBounce", true));
    }

    /// <summary>
    /// Turns the statue in level 1 when it is colored
    /// </summary>
    private void TurnStatue() {
        statueTurned = true;
        animatorController = GetComponent<Animator>();
        
        StartCoroutine(QueueAnimation("StatueTurnBack", "StatueTurn"));
        
    }

    /// <summary>
    /// Turns the statue back when color is removed from it
    /// </summary>
    private void TurnStatueBack() {
        if (statueTurned) {
            Debug.Log("StatueTurned");
            statueTurned = false;
            animatorController = GetComponent<Animator>();
            StartCoroutine(QueueAnimation("StaturTurn", "StatueTurnBack"));
            
        }
    }

    /// <summary>
    /// Used to check of the current animation has finished playing 
    /// </summary>
    /// <param name="animName">The name of the animation</param>
    /// <returns></returns>
    private bool HasFinishedAnim(string animName) {
        if (animatorController.GetBool(animName) == false && animatorController.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.95f)
            return true;
        else
            return false;
    }

    /// <summary>
    /// Used to wait for a given animation to finish
    /// </summary>
    /// <param name="animation">Name of animation</param>
    /// <returns></returns>
    IEnumerator WaitForAnim(string animation, bool shouldDisable) {
        yield return new WaitForSeconds(0.2f);
        while (HasFinishedAnim(animation) == false) {
            yield return new WaitForEndOfFrame();
        }

        if(shouldDisable)
            DisableObject();
    }

    /// <summary>
    /// Waits for the current animation to finish then plays the next one after
    /// </summary>
    /// <param name="currentAnimation">The animation that is currently playing</param>
    /// <param name="nextAnimation"> The animation that should be Queued</param>
    /// <returns></returns>
    IEnumerator QueueAnimation(string currentAnimation, string nextAnimation) {
        yield return new WaitForSeconds(0.2f);
        while(HasFinishedAnim(currentAnimation) == false) {
            yield return new WaitForEndOfFrame();
        }

        animatorController.Play(nextAnimation);
    }

    /// <summary>
    /// Disables the gameobject
    /// </summary>
    private void DisableObject() {
        gameObject.SetActive(false);
    }
}
