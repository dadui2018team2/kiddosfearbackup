﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class whaleFlying : MonoBehaviour {




    
    public Vector3 center;
    public Vector3 axis;
    private Vector3 startPos;
    public float radius;
    public float radiusSpeed;
    public float rotationSpeed;
    public float yOffsetFromZero;
    public float ySmoothen;

    public AnimationCurve animationCurve;

    public bool runCoroutine;

    private void Awake() {
        center = new Vector3(transform.position.x + radius, transform.position.y, transform.position.z + radius);
    }

    private void Start() {

        //StartCoroutine(StartRotation());

        if(transform.position == Vector3.zero) {
            transform.position = new Vector3(-20, 0, -20);
        }
        
    }
    
    IEnumerator Orbit() {
        runCoroutine = true;
        bool goingUp = true;
        float timeScale = 0;

        while (runCoroutine) {
            if (timeScale > 1f) {
                timeScale = 0;
                goingUp = !goingUp;
            }
            else {
                timeScale += Mathf.Clamp(Time.deltaTime * ySmoothen, 0, 1);
            }


            if (goingUp) {
                axis.z = animationCurve.Evaluate(timeScale)*yOffsetFromZero;
            }
            else {
                axis.z = -animationCurve.Evaluate(timeScale)*yOffsetFromZero;
            }


            transform.RotateAround(center, axis, rotationSpeed * Time.deltaTime);
            Vector3 desiredPosition = (transform.position - center).normalized * radius + center;

            Vector3 rotDir = Vector3.Cross(transform.position - center, axis);
            transform.rotation = Quaternion.LookRotation(rotDir, axis);

            transform.position = Vector3.MoveTowards(transform.position, desiredPosition, radiusSpeed * Time.deltaTime);
            //transform.LookAt(FindObjectOfType<PlayerMovement>().gameObject.transform);  
            yield return new WaitForEndOfFrame();
        }

    }

    IEnumerator StartRotation() {
        Vector3 rotDir = Vector3.Cross(transform.position - center, axis);
        int counter = 0;

        while (transform.rotation != Quaternion.LookRotation(rotDir, axis) || counter < 100) {
            
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(rotDir, axis), Time.deltaTime * 15);

            counter++;
            yield return new WaitForEndOfFrame();
        }

        counter = 0;

        StartCoroutine(Orbit());
    }

    public void StopOrbit() {
        runCoroutine = false;
    }

    public void StartOrbit() {
        StartCoroutine(StartRotation());
    }
    
}
    /*float timecounter = 0;

    public float width;
    public float height; 
    public float speed;
    public float freq;

    public bool goingUp;
    AnimationCurve yAxisMovement;
	

	void Update () {

        timecounter += Time.deltaTime * speed;
        
        float x = Mathf.Sin(timecounter) * width;
        float y = Mathf.Sin(timecounter) * freq;
        float z = Mathf.Cos(timecounter) * height;

        transform.position = new Vector3(x, y, z);
        
        // Rotating the object towards the camera
        transform.LookAt(Camera.main.transform.position, Vector3.up);
    } */
