﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalPickup : MonoBehaviour {

    public Animator bossAnimController;
    public GameObject camEndPosition;
    public float fadeStep;

    private CameraAnimation camAnim;
    private UIGameManager uiControl;
    private CameraController camControl;
    private TouchPaint touchPaint;
    private bool panning = false;


	// Use this for initialization
	void Start () {
        bossAnimController.speed = 0;

        camAnim = FindObjectOfType<CameraAnimation>();
        uiControl = FindObjectOfType<UIGameManager>();
        camControl = FindObjectOfType<CameraController>();
        touchPaint = FindObjectOfType<TouchPaint>();

	}

    private void OnTriggerEnter(Collider other) {
        if (!panning) {
            panning = true;
            camAnim.Pan(GameManager.Player.transform, camEndPosition.transform, 1f, 0f, false);
            uiControl.MoveLeft(false);
            uiControl.MoveRight(false);
            bossAnimController.speed = 1;
            StartCoroutine(WaitForPan());
        }

    }

    IEnumerator WaitForPan() {
        int counter = 0;
        while (camAnim.isPanning || counter < 100) {
            yield return new WaitForEndOfFrame();
            counter++;
        }
        counter = 0;

        uiControl.ShowGUI(false);
        touchPaint.enabled = false;

        camControl._world = camEndPosition;

        yield return new WaitForSeconds(2f);

        camAnim.BeginFade(0f, 1f, fadeStep);

        while (camAnim.isFading) {
            yield return new WaitForEndOfFrame();
        }

        SceneManager.LoadScene("Credits");
    }
}
