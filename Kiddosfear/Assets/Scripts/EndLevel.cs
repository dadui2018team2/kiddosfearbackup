﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class EndLevel : MonoBehaviour {

    /*
	This script is for ending the level when player enters this object's trigger. Works in cooperation with GameManager (EndLevel()).
	 */

    private GameObject player;
    private ActionAnimator actionAnimator;
    private Coroutine waitTimer;

    public GameObject startPanPos;
    private bool waitingForPan;

    private void Awake() {
        actionAnimator = FindObjectOfType<ActionAnimator>();
        waitingForPan = false;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            player = other.gameObject;

            if (waitTimer == null) {
                waitTimer = StartCoroutine(WaitForAnimation());
            }
        }
	}

    /// <summary>
    /// Starts the death-animation and waits for it to finish before reloading the scene.
    /// </summary>
    private IEnumerator WaitForAnimation() {
        player.transform.Find("LevelEndParticles").gameObject.SetActive(true);
        AudioPlayer.PlayEvent("EndLevelFeedback", player, 3f);
        actionAnimator.TriggerCelebrationAnim();
        player.GetComponent<PlayerMovement>().enabled = false;
        //player.GetComponent<Shooting>().enabled = false;
        FindObjectOfType<TouchPaint>().gameObject.SetActive(false);
        CameraController camControl = FindObjectOfType<CameraController>();

        while (actionAnimator.HasFinishedAnimation("celebration") == false)
            yield return new WaitForEndOfFrame();

        StartCoroutine(PlayerExitDoor());

        if (startPanPos == null) {
            startPanPos = GameManager.Player;
        }

        CameraAnimation camAnim = FindObjectOfType<CameraAnimation>();
        Transform panPos = GameObject.FindWithTag("LevelExitPanPos").transform;

        Debug.Log("panPos != null: " + panPos);
        UIGameManager uiControl = FindObjectOfType<UIGameManager>();
        uiControl.MoveLeft(false);
        uiControl.MoveRight(false);
        if (!waitingForPan) {
            camControl._world = startPanPos;
            yield return new WaitForSeconds(1f);
            camAnim.Pan(startPanPos.transform, panPos, 1f, 0f, true);
        }

        if (!waitingForPan) {
            StartCoroutine(WaitForPan(camAnim));
        }



        if (PlayerPrefs.HasKey("CompletedLevels")) {
            int currentLevel = GetSceneValue();

            if (currentLevel > PlayerPrefs.GetInt("CompletedLevels")) {
                PlayerPrefs.SetInt("CompletedLevels", currentLevel);
            }
        }


        string level = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetString("levelprogression", level);
        Debug.Log("Level completed: " + level);


        while (waitingForPan) {
            yield return new WaitForEndOfFrame();
        }
        waitTimer = null;
        GameManager.CurrentGameState = GameManager.GameState.End;
    }

    IEnumerator PlayerExitDoor() {
        PlayerMovement playerMovement = FindObjectOfType<PlayerMovement>();
        playerMovement.walkThroughDoor = true;
        while (Vector3.Distance(transform.position, playerMovement.gameObject.transform.position) > 2f) {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(.25f);
        playerMovement.walkThroughDoor = false;
        playerMovement.enabled = false;
    }
    
    IEnumerator WaitForPan(CameraAnimation camAnim) {
        waitingForPan = true;
        int counter = 0;
        while (camAnim.isPanning || counter < 100) {
            yield return new WaitForEndOfFrame();
            Debug.Log("Is panning: " + camAnim.isPanning);
            counter++;
        }

        counter = 0;
        
        waitingForPan = false;
    }

    private int GetSceneValue() {
        string currentSceneName = SceneManager.GetActiveScene().name;
        currentSceneName = currentSceneName.Substring(5, 1);
        int currentLevel = (int)char.GetNumericValue(currentSceneName[0]);
        if (currentLevel == -1) Debug.Log("I couldn't find a number at 6th position of the level, instead it was: " + currentSceneName);
        return currentLevel;
    }
}
