﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupBehaviour : MonoBehaviour {

    [Header("General settings")]
    [Tooltip("The rotation speed of the pickup in degrees pr. second")]
    public int rotationSpeed;
    public float bobbingFrequency;
    public float bobbingAmplitude;
    [Tooltip("The particles that will play when the player triggers this pickup.")]
    public ParticleSystem pickupFeedback;

    [Header("Color pickup specifics")]
    [Tooltip("Leave empty if not a color pickup (Should be either 'Red', 'Green' or 'Blue' without <'>)")]
    public string objectColor;

    [Header("Key pickup specifics")]
    public bool isKeyPart;
    public int keyPartIndex;
    private InGameDoors exitDoor;

    private GameObject mesh;

    private Vector3 originalPosition;
    private Vector3 temporaryPosition;

    private Coroutine feedbackRoutine;

    private void Awake() {
        exitDoor = GameObject.FindWithTag("ExitDoor").GetComponent<InGameDoors>();

        mesh = GetComponentInChildren<MeshRenderer>().gameObject;
        originalPosition = mesh.transform.position;

        SnapToGrid gridSnap = GetComponent<SnapToGrid>();
        if (gridSnap)
            gridSnap.enabled = false;
    }

    // Update is called once per frame
    void Update () {
        transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed, Space.World);

        temporaryPosition = originalPosition;
        temporaryPosition.y += Mathf.Sin(Time.fixedTime * Mathf.PI * bobbingFrequency) * bobbingAmplitude;
        mesh.transform.position = temporaryPosition;
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player" && feedbackRoutine == null) {
            if (isKeyPart) {
                exitDoor.AddKeyPart(keyPartIndex);
                FindObjectOfType<UIGameManager>().IncreasePickupCounter();

                feedbackRoutine = StartCoroutine(GiveFeedback("PickUpLight"));
            }
            else if (objectColor != "") {
                FindObjectOfType<UIGameManager>().EnableColor(objectColor);
                FindObjectOfType<UIGameManager>().SelectColor(objectColor);

                //TEMPORARY! Enables the erase-button whenever you pick up a color (so you won't ever be stuck with colors without being able to erase)
                FindObjectOfType<UIGameManager>().EnableColor("");

                switch (objectColor) {
                    case "Red":
                        pickupFeedback.startColor = Color.red;
                        break;
                    case "Green":
                        pickupFeedback.startColor = Color.green;
                        break;
                    case "Blue":
                        pickupFeedback.startColor = Color.blue;
                        break;
                    default:
                        return;
                }

                feedbackRoutine = StartCoroutine(GiveFeedback("ColorPickUps"));
            }
            else {
                Debug.LogError("Pickup is not a key and does not contain a valid color value.");
            }
        }
    }

    private IEnumerator GiveFeedback(string audioFeedback) {
        GetComponentInChildren<MeshRenderer>().gameObject.SetActive(false);

        AudioPlayer.PlayEvent(audioFeedback, gameObject, 3);
        pickupFeedback.Play();

        while (pickupFeedback.isPlaying)
            yield return new WaitForEndOfFrame();
        
        gameObject.SetActive(false);
    }
}
