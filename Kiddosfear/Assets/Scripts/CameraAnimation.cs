﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : MonoBehaviour {

    private GameObject focusPoint;

    [HideInInspector] public bool isFading;
    [HideInInspector] public bool isPanning;
    [HideInInspector] public bool pausePan;

    [Header("Camera panning")]
    [SerializeField] float waitTimeBeforePanning;
    [SerializeField] float maxPanSpeed;
    [SerializeField] float panSmoothening;

    [Header("Fading")]
    [SerializeField] Color fadeColor;
    [Range(0f, 1f)]
    public float fadeStep;

    //References from scene
    private UIGameManager uiControl;
    private CameraController camControl;
    private PlayerMovement playerMovement;
    private TouchPaint touchPaint;

    private Transform player;
    private Transform exitDoor;

    //Fading
    Texture2D overlay;
    Rect screen;

    private void Awake() {
        //Getting the references in the scene
        uiControl = FindObjectOfType<UIGameManager>();
        camControl = GetComponent<CameraController>();
        playerMovement = FindObjectOfType<PlayerMovement>();
        touchPaint = FindObjectOfType<TouchPaint>();

        player = GameObject.FindWithTag("Player").transform;
        exitDoor = GameObject.FindWithTag("ExitDoor").transform;

        //Creating a gameobject for panning around the scene
        focusPoint = new GameObject();
        focusPoint.name = "CameraPanningFocus";

        //Creating variables for fading
        overlay = new Texture2D(1, 1);
        screen = new Rect(0, 0, Screen.width, Screen.height);
    }

    /// <summary>
    /// Start fading out. Check whether the fade is finished with "CameraAnimation.isFading".
    /// </summary>
    public void BeginFade(float startAlpha, float endAlpha, float fadeStepTime) {
        StartCoroutine(Fade(startAlpha, endAlpha, fadeStepTime));
    }

    /// <summary>
    /// Start a pan towards a given position.
    /// </summary>
    /// <param name="start">What object the camera is currently focusing on.</param>
    /// <param name="end">What object the camera should end up focusing on.</param>
    /// <param name="waitTime">Time to wait before beginning to pan. Leave as 0f to immediately start panning.</param>
    /// <param name="dampValue">How much dampening there should be. Leave as 0f to use the default value from the inspector..</param>
    /// <param name="reEnableUI">Should the UI be re-enabled after the pan is done.</param>
    public void Pan(Transform start, Transform end, float waitTime, float dampValue, bool reEnableUI) {
        if (dampValue == 0f)
            dampValue = panSmoothening;

        StartCoroutine(PanBetween(start, end, waitTime, dampValue, reEnableUI));
    }

    private void OnGUI() {
        if (isFading) {
            //Drawing the fade overlay on the screen
            GUI.DrawTexture(screen, overlay);
            overlay.SetPixel(0, 0, fadeColor);
            overlay.Apply();
        }
    }

    private IEnumerator PanBetween(Transform start, Transform end, float waitTime, float dampValue, bool reEnableUI) {
        isPanning = true;

        uiControl.ShowGUI(false);
        touchPaint.enabled = false;

        focusPoint.transform.position = start.position;

        camControl._player = focusPoint;
        camControl._world = focusPoint;

        //Moving the camera to approximatly the position it's supposed to have when starting the level
        transform.position = new Vector3(focusPoint.transform.position.x, focusPoint.transform.position.y, transform.position.z);
        camControl.focusPointPosition = focusPoint.transform.position;

        //Waiting with panning for the specified duration
        float waited = 0f;
        while (waited < waitTime) {
            waited += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        Vector3 velocity = Vector3.zero;

        while (Vector3.Distance(focusPoint.transform.position, end.position) > 0.1f) {
            if (!pausePan)
                focusPoint.transform.position = Vector3.SmoothDamp(focusPoint.transform.position, end.position, ref velocity, dampValue, maxPanSpeed);

            yield return new WaitForEndOfFrame();
        }

        camControl._player = player.gameObject;
        camControl._world = player.gameObject;

        if (reEnableUI) {
            touchPaint.enabled = true;
            uiControl.ShowGUI(true);
        }

        isPanning = false;
    }

    private IEnumerator Fade(float startAlpha, float finalAlpha, float fadeStep) {
        isFading = true;

        //Creating an overlay to put on the camera for fading effect
        fadeColor.a = startAlpha;
        Texture2D overlay = new Texture2D(1, 1);
        overlay.SetPixel(0, 0, fadeColor);
        overlay.Apply();

        float currentAlpha = startAlpha;
        float lerp = 0f;

        while (currentAlpha != finalAlpha) {
            //Changing the alpha value of the fade color
            currentAlpha = Mathf.Lerp(startAlpha, finalAlpha, lerp);
            fadeColor.a = currentAlpha;

            lerp += fadeStep;

            yield return new WaitForEndOfFrame();
        }

        isFading = false;
    }
}
