﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelDoorManager : MonoBehaviour {

    private void Start() {
        if (PlayerPrefs.HasKey("CompletedLevels")) {
            int activeLevels = PlayerPrefs.GetInt("CompletedLevels") + 1;
            foreach (Transform child in transform) {
                if (child.GetComponent<LevelDoorInteraction>().doorLevel <= activeLevels) {
                    child.GetComponent<LevelDoorInteraction>().ActivateAndOpenDoor();
                }
            }
        }
        else {
            PlayerPrefs.SetInt("CompletedLevels", 0);
            foreach (Transform child in transform) {
                if (child.GetComponent<LevelDoorInteraction>().doorLevel == 1) {
                    child.GetComponent<LevelDoorInteraction>().ActivateAndOpenDoor();
                }
            }
        }
    }

    private void Update() {
        if (Input.GetKey(KeyCode.Alpha0)) {
            PlayerPrefs.SetInt("CompletedLevels", 0);
        }
        if (Input.GetKey(KeyCode.Alpha1)) {
            PlayerPrefs.SetInt("CompletedLevels", 1);
        }
        if (Input.GetKey(KeyCode.Alpha2)) {
            PlayerPrefs.SetInt("CompletedLevels", 2);
        }
        if (Input.GetKey(KeyCode.Alpha3)) {
            PlayerPrefs.SetInt("CompletedLevels", 3);
        }
        if (Input.GetKey(KeyCode.Alpha4)) {
            PlayerPrefs.SetInt("CompletedLevels", 4);
        }
    }

}


    /* [Header("Updated in Awake")]
    public GameObject doorIn;
    public List<GameObject> teleDoors;
    public int currentlyActiveDoorIndex;
    public DoorInteraction currentlyActiveDoor;

    private void Awake() {
        
        teleDoors = teleDoors.OrderBy(go => go.name).ToList();

        if (teleDoors.Count == 0) {
            Debug.Log("There are no output doors");
        }
        if (doorIn == null) {
            Debug.Log("There is no input door");
        }
    }

    public void Start() {
        if (CheckHasDoorCheckPoint() && LoadDoorCheckpoint() != 0) {
            currentlyActiveDoorIndex = LoadDoorCheckpoint();
            //AudioPlayer.LoadBank("TeleDoor");
            for (int i = currentlyActiveDoorIndex - 1; i >= 0; i--) {
                GameObject currentDoor = teleDoors[i].GetComponentInChildren<DoorInteraction>().gameObject;
                int currentDoorValue = (int)char.GetNumericValue(currentDoor.name[currentDoor.name.Length - 1]);
                if (currentDoorValue != currentlyActiveDoorIndex) {
                    currentDoor.GetComponent<DoorInteraction>().ActivateAndOpenDoor();
                    currentDoor.GetComponent<DoorInteraction>().SetOtherDoor(teleDoors[currentlyActiveDoorIndex - 1]);
                }
                else {
                    currentDoor.GetComponent<DoorInteraction>().ActivateDoor();
                    currentDoor.GetComponent<DoorInteraction>().SetOtherDoor(teleDoors[currentlyActiveDoorIndex - 1]);
                }
            }
            if (LoadDoorCheckpoint() > 0) {
                doorIn.GetComponent<DoorInteraction>().ActivateAndOpenDoor();
                doorIn.GetComponent<DoorInteraction>().SetOtherDoor(teleDoors[currentlyActiveDoorIndex - 1]);
            }
        }
        else {
            SaveDoorCheckpoint(0);
        }
    }

    /// <summary>
    /// Checks if a door is the new checkpoint.
    /// </summary>
    /// <param name="door">Door to check.</param>
    /// <returns></returns>
    public bool DoorIsNewCheckpoint(GameObject door) {
        if (!door.GetComponent<DoorInteraction>().isActive) {
            if ((int)char.GetNumericValue(door.name[door.name.Length - 1]) > currentlyActiveDoorIndex) {
                return true;
            }
            return false;
        }
        return true;
    }

    /// <summary>
    /// Activate all previous doors from the input door. (If a checkpoint was skipped, it is activated as well)
    /// </summary>
    /// <param name="door">Last checkpoint to activate.</param>
    public void ActivateDoors(LevelDoorInteraction door) {
        int currentDoorNumber = (int)char.GetNumericValue(door.gameObject.name[door.gameObject.name.Length - 1]);
        //AudioPlayer.SetSwitch("TeleDoor", "TeleDoorAppear", door.gameObject);
        //AkSoundEngine.PostEvent("TeleDoor", door.gameObject);

        print(teleDoors[currentDoorNumber - 1]);
        SetNewCurrentDoor(teleDoors[currentDoorNumber - 1]);

        for (int i = currentDoorNumber-1; i >= 0; i--) {
            DoorInteraction doorInteraction = teleDoors[i].GetComponent<DoorInteraction>();
            doorInteraction.SetOtherDoor(currentlyActiveDoor.gameObject);
            if (doorInteraction.isActive == false) {
                doorInteraction.ActivateDoor();
            }
            else {
                doorInteraction.OpenDoor();
            }
        }

        DoorInteraction doorInDI = doorIn.GetComponent<DoorInteraction>();
        doorInDI.SetOtherDoor(currentlyActiveDoor.gameObject);
        if (doorInDI.isActive == false) {
            doorIn.GetComponent<DoorInteraction>().ActivateDoor();
            doorIn.GetComponent<DoorInteraction>().Invoke("OpenDoor", 1f);
        }
    }

    /// <summary>
    /// Returns the first door in the scene.
    /// </summary>
    /// <returns></returns>
    public string GetDoorInName() {
        return doorIn.name;
    }

    /// <summary>
    /// Sets current checkpoint.
    /// </summary>
    /// <param name="door">Door to set.</param>
    private void SetNewCurrentDoor(GameObject door) {
        currentlyActiveDoor = door.GetComponent<DoorInteraction>();
    }

    /// <summary>
    /// Check if the PlayerPref for the current scene has a checkpoint.
    /// </summary>
    /// <returns>true if key exists.</returns>
    public bool CheckHasDoorCheckPoint() {
        string saveName = GetPlayerPrefCheckpointName();
        return PlayerPrefs.HasKey(saveName);
    }
    
    /// <summary>
    /// Saves new int value for the current scene's PlayerPref.
    /// </summary>
    /// <param name="value"></param>
    public void SaveDoorCheckpoint(int value) {
        string saveName = GetPlayerPrefCheckpointName();
        PlayerPrefs.SetInt(saveName, value);
    }
    
    /// <summary>
    /// Loads int for current scene's PlayerPref.
    /// </summary>
    /// <returns></returns>
    public int LoadDoorCheckpoint() {
        string saveName = GetPlayerPrefCheckpointName();
        return PlayerPrefs.GetInt(saveName);
    }
    
    /// <summary>
    /// Returns name of current scene's PlayerPref.
    /// </summary>
    /// <returns></returns>
    public string GetPlayerPrefCheckpointName() {
        return "level" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + "Checkpoint";
    }

    /// <summary>
    /// Sets PlayerPref to 0
    /// </summary>
    public void ResetPlayerPref() {
        SaveDoorCheckpoint(0);
    }

    private void OnDestroy() {
        int bankUnload;
        AkSoundEngine.UnloadBank("TeleDoor", (System.IntPtr)0, out bankUnload);
    }
}
*/
